﻿using Applications.Commons;
using Applications.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardService _service;

        public DashboardController(IDashboardService service)
        {
            _service = service;
        }

        [HttpGet]
        public Response GetCustomer()
        {
            return _service.GetCustomer();
        }

        [HttpGet]
        public Response GetCustomerInMonth()
        {
            return _service.GetCustomerInMonth();
        }

        [HttpGet]
        public Response GetOrder()
        {
            return _service.GetOrder();
        }

        [HttpGet]
        public Response GetOrderInMonth()
        {
            return _service.GetOrderInMonth();
        }

        [HttpGet]
        public Response GetProvider()
        {
            return _service.GetProvider();
        }

        [HttpGet]
        public Response GetProviderInMonth()
        {
            return _service.GetProviderInMonth();
        }

        [HttpGet]
        public Response GetUserInMonth()
        {
            return _service.GetUserInMonth();
        }
    }
}
