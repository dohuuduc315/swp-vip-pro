﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MiniServiceController : ControllerBase
    {
        private readonly IMiniServiceService _service;

        public MiniServiceController(IMiniServiceService service)
        {
            _service = service;
        }

        [HttpPut]
        public async Task<Response> Update(int id, MiniServiceViewModel model)
        {
            return await _service.UpdateMiniService(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.DeleteMiniService(id);
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllMiniService(pageIndex, pageSize);
        }
    }
}
