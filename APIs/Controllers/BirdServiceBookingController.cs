﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BirdServiceBookingController : ControllerBase
    {
        private readonly IBirdServiceBookingService _service;

        public BirdServiceBookingController(IBirdServiceBookingService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> GetByCustomerId(int id)
        {
            return await _service.GetByCustomerId(id);
        }

        [HttpGet]
        public async Task<Response> GetByProviderId(int id)
        {
            return await _service.GetByProviderId(id);
        }

        [HttpGet]
        public async Task<Response> GetBookingInfoById(int id)
        {
            return await _service.GetBookingByid(id);
        }

        [HttpPost]
        public async Task<Response> CreateBooking(CreateBookingViewModel model)
        {
            return await _service.CreateBooking(model);
        }


        [HttpGet]
        public async Task<Response> GetAllBooking(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllServiceBooking(pageIndex, pageSize);
        }

        [HttpPut]
        public async Task<Response> PayBooking(int id)
        {
            return await _service.PayBooking(id);
        }

        [HttpPut]
        public async Task<Response> CancelBooking(int id)
        {
            return await _service.CancelBooking(id);
        }
    }
}
