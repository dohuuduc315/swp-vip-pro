﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProviderController : ControllerBase
    {
        private readonly IProviderService _service;

        public ProviderController(IProviderService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllProvider(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetProviderInfor(id);
        }

        [HttpGet]
        public async Task<Response> GetByProviderId(int id)
        {
            return await _service.GetByProviderId(id);
        }

        [HttpGet]
        public async Task<Response> GetAllProviderUnauthorized(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllProviderUnauthorized(pageIndex, pageSize);
        }

        [HttpPost]
        public async Task<Response> Create(ProviderViewModel model)
        {
            return await _service.CreateProvider(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, ProviderViewModel model)
        {
            return await _service.UpdateProvider(id, model);
        }

        //[HttpDelete]
        //public async Task<Response> Delete(int id)
        //{
        //    return await _service.RemoveProvider(id);
        //}

        [HttpPost]
        public async Task<Response> VerifyProvider(int id)
        {
            return await _service.VerifyProvider(id);
        }

        [HttpPost]
        public async Task<Response> BanProvider(int id)
        {
            return await _service.BanProvider(id);
        }

        [HttpGet]
        public Response GetServiceCreated(int id)
        {
            return _service.GetServiceCreated(id);
        }

        [HttpGet]
        public Response GetServiceCreatedInMonth(int id)
        {
            return _service.GetServiceCreatedInMonth(id);
        }

        [HttpGet]
        public Response GetPrice(int id)
        {
            return _service.GetPrice(id);
        }

        [HttpGet]
        public Response GetPriceInMonth(int id)
        {
            return _service.GetPriceInMonth(id);
        }

        [HttpGet]
        public Response GetBooking(int id)
        {
            return _service.GetBooking(id);
        }

        [HttpGet]
        public Response GetBookingInMonth(int id)
        {
            return _service.GetBookingInMonth(id);
        }

        [HttpGet]
        public async Task<Response> GetByProviderName(string name, int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetByName(name, pageIndex, pageSize);
        }
    }
}
