﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _service;

        public CustomerController(ICustomerService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllCustomer(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetCustomerInfor(id);
        }

        [HttpGet]
        public async Task<Response> GetByCustonerId(int id)
        {
            return await _service.GetByCustomerId(id);
        }

        [HttpPost]
        public async Task<Response> Create(CustomerViewModel model)
        {
            return await _service.CreateCustomer(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, CustomerViewModel model)
        {
            return await _service.UpdateCustomer(id, model);
        }

        //[HttpDelete]
        //public async Task<Response> Delete(int id)
        //{
        //    return await _service.RemoveCustomer(id);
        //}
    }
}
