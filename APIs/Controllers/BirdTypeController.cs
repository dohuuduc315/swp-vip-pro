﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BirdTypeController : ControllerBase
    {
        private readonly IBirdTypeService _service;

        public BirdTypeController(IBirdTypeService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllBirdType(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetById(id);
        }

        [HttpPost]
        public async Task<Response> Create(BirdTypeViewModel model)
        {
            return await _service.Create(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, BirdTypeViewModel model)
        {
            return await _service.UpdateBirdType(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.DeleteBirdType(id);
        }
    }
}
