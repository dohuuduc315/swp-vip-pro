﻿using Applications.Commons;
using Applications.Interfaces;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PriceController : ControllerBase
    {
        private readonly IPriceService _service;

        public PriceController(IPriceService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllPrice(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetPriceInfor(id);
        }

        [HttpGet]
        public async Task<Response> FilterBirdService(Location location, decimal StartPrice, decimal EndPrice, double rating, string Category, string BirdServiceName)
        {
            return await _service.Filter(location, StartPrice, EndPrice, rating, Category, BirdServiceName);
        }

        [HttpPost]
        public async Task<Response> CreatePrice(Price model)
        {
            return await _service.CreatePrice(model);
        }

        [HttpPut]
        public async Task<Response> UpdatePrice(int id, Price model)
        {
            return await _service.UpdatePrice(id, model);
        }


        [HttpDelete]
        public async Task<Response> DeletePrice(int id)
        {
            return await _service.RemovePrice(id);
        }
    }
}
