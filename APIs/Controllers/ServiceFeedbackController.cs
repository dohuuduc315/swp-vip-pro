﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ServiceFeedbackController : ControllerBase
    {
        private readonly IServiceFeedbackService _service;

        public ServiceFeedbackController(IServiceFeedbackService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllServiceFeedback(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetServiceFeedbackInfor(id);
        }

        [HttpGet]
        public async Task<Response> GetByBirdServiceId(int id)
        {
            return await _service.GetByBirdServiceId(id);
        }

        [HttpPost]
        public async Task<Response> Create(ServiceFeedbackViewModel model)
        {
            return await _service.CreateServiceFeedback(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, ServiceFeedbackViewModel model)
        {
            return await _service.UpdateServiceFeedback(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.RemoveServiceFeedback(id);
        }
    }
}
