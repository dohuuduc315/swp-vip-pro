﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ServiceCategoryController : ControllerBase
    {
        private readonly IServiceCategoryService _service;

        public ServiceCategoryController(IServiceCategoryService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllServiceCategory(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetServiceCategoryInfor(id);
        }

        [HttpGet]
        public async Task<Response> GetByServiceType(ServiceType type)
        {
            return await _service.GetServiceCategoryByType(type);
        }

        [HttpPost]
        public async Task<Response> Create(ServiceCategoryViewModel model)
        {
            return await _service.CreateServiceCategory(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, ServiceCategoryViewModel model)
        {
            return await _service.UpdateServiceCategory(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.RemoveServiceCategory(id);
        }
    }
}
