﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _service;

        public CommentController(ICommentService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllComment(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetCommentInfor(id);
        }

        [HttpGet]
        public async Task<Response> GetByProviderId(int id, int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetByProviderId(id, pageIndex, pageSize);
        }

        [HttpPost]
        public async Task<Response> Create(CommentViewModel model)
        {
            return await _service.CreateComment(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, CommentViewModel model)
        {
            return await _service.UpdateComment(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.RemoveComment(id);
        }
    }
}
