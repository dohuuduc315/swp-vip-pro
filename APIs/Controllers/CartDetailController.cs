﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Services;
using Applications.ViewModels;
using Applications.ViewModels.Cart;
using Applications.ViewModels.CartDetail;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CartDetailController : ControllerBase
    {
        private readonly ICartDetailService _service;
        public CartDetailController(ICartDetailService service)
        {
            _service = service;
        }

        [HttpDelete]
        public async Task<Response> DeleteCartDetail(int id)
        {
            return await _service.RemoveCartDetail(id);
        }

        [HttpPost]
        public async Task<Response> UpdateCartDetail(int id,UpdateCartDetailViewModel model)
        {
            return await _service.UpdateCartDetail(id,model);
        }

        [HttpPost]
        public async Task<Response> CreateCartDetail(CreateCartDetailViewModel model)
        {
            return await _service.CreateCartDetail(model);
        }
    }
}