﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Services;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        private readonly IWalletService _service;

        public WalletController(IWalletService service)
        {
            _service = service;
        }

        [HttpGet]
        public Task<Response> GetByUserId(int id)
        {
            return _service.GetByUserId(id);
        }

        [HttpPut]
        public async Task<Response> Update(int id, string stk, Bank bank)
        {
            var result = await _service.UpdateBankToWallet(id, stk, bank);
            return result;
        }
    }
}
