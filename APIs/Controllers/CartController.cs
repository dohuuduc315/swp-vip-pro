using Applications.Interfaces;
using Microsoft.AspNetCore.Http;
﻿using Applications.Commons;
using Applications.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartService _service;

        public CartController(ICartService service)
        {
            _service = service;
        }
    

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllCart(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetByUserId(int id)
        {
            return await _service.GetCartByUserId(id);
        }
    }
}

