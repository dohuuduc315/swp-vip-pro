﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportService _service;

        public ReportController(IReportService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetAllReport(pageIndex, pageSize);
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetReportInfor(id);
        }

        [HttpPost]
        public async Task<Response> Create(CreateReportViewModel model)
        {
            return await _service.CreateReport(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, UpdateReportViewModel model)
        {
            return await _service.UpdateReport(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.RemoveReport(id);
        }

        [HttpGet]
        public async Task<Response> GetByUserId(int id, int pageIndex = 0, int pageSize = 10)
        {
            return await _service.GetReportByUserId(id, pageIndex, pageSize);
        }

        [HttpPut]
        public async Task<Response> UpdateReportStatus(int id, ReportStatus status)
        {
            return await _service.UpdateReportStatus(id, status);
        }
    }
}
