﻿using Applications.Commons;
using Applications.Interfaces;
using Domain.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BookingDetailController : ControllerBase
    {
        private readonly IBookingDetailService _service;

        public BookingDetailController(IBookingDetailService service)
        {
            _service = service;
        }

        [HttpPut]
        public async Task<Response> UpdateBookingDetailWorkingStatus(int id, WorkingStatus status)
        {
            return await _service.UpdateBookingWorkingStatus(id,status);
        }

        [HttpGet]
        public async Task<Response> GetBookingDetailById(int id)
        {
            return await _service.GetBookingDetailByid(id);
        }
    }
}