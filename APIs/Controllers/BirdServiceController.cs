﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BirdServiceController : ControllerBase
    {
        private readonly IBirdServiceService _service;

        public BirdServiceController(IBirdServiceService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<Response> GetById(int id)
        {
            return await _service.GetBirdServiceById(id);
        }

        [HttpGet]
        public async Task<Response> GetHighestAvgratingBirdService()
        {
            return await _service.GetHighestBSV();
        }

        [HttpGet]
        public async Task<Response> GetByProviderId(int id)
        {
            return await _service.GetBirdServiceByProviderId(id);
        }

        [HttpGet]
        public async Task<Response> GetAllService()
        {
            return await _service.GetAllBirdService();
        }

        [HttpPost]
        public async Task<Response> Create(CreateServiceViewModel model)
        {
            return await _service.CreateBirdService(model);
        }

        [HttpPut]
        public async Task<Response> Update(int id, UpdateServiceViewModel model)
        {
            return await _service.UpdateBirdService(id, model);
        }

        [HttpDelete]
        public async Task<Response> Delete(int id)
        {
            return await _service.RemoveBirdService(id);
        }
    }
}
