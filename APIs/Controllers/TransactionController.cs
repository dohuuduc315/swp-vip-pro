﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        public readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        // GET: api/Transaction
        [HttpGet]
        public async Task<Response> Get(int pageIndex = 0, int pageSize = 10)
        {
            var result = await _transactionService.GetAllTransaction(pageIndex, pageSize);
            return result;
        }
        // GET api/Transaction/5
        [HttpGet("{id}")]
        public async Task<Response> GetById(int id)
        {
            Response result = await _transactionService.GetTransactionInfor(id);
            return result;
        }

        [HttpGet]
        public async Task<Response> GetByStatus([FromQuery] TransactionStatus status)
        {
            Response result = await _transactionService.GetTransactionByStatus(status);
            return result;
        }

        [HttpGet]
        public async Task<Response> GetByUserId([FromQuery] int id)
        {
            Response result = await _transactionService.GetTransactionByUserId(id);
            return result;
        }

        [HttpPut]
        public async Task<Response> AuthorizeTransaction([FromQuery] int id, TransactionStatus status)
        {
            Response result = await _transactionService.AuthorizeTransaction(id, status);
            return result;
        }

        // POST api/Transaction
        [HttpPost]
        public async Task<Response> Create(CreateTransactionViewModel model)
        {
            var result = await _transactionService.CreateTransaction(model);
            return result;
        }

        // PUT api/Transaction/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Response>> Update(int id, TransactionViewModel model)
        {
            var result = await _transactionService.UpdateTransaction(id, model);
            return result;
        }

        // DELETE api/Transaction/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Response>> Delete(int id)
        {
            var report = await _transactionService.GetAllTransaction(id);
            if (report == null) return NotFound();
            await _transactionService.RemoveTransaction(id);
            return NoContent();
        }
    }
}
