using APIs;
using APIs.Middlewares;
using Applications.Commons;
using Infrastructures;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.BuildServices(builder.Configuration);
builder.Services.AddInfrastructuresService();

//var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

// add cors
//builder.Services.AddCors(options =>
//{
//    options.AddPolicy(MyAllowSpecificOrigins, policy =>
//    {
//        policy.WithOrigins("https://bird-service.vercel.app", "https://localhost:5173")
//        .AllowAnyHeader()
//              .AllowAnyMethod();
//    });
//});
// add authen policy
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => builder.Configuration.Bind("JWTSection", options));

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(IdentityData.Member, policy => policy.RequireRole("Admin", "Customer", "System"));
    options.AddPolicy(IdentityData.Admin, policy => policy.RequireRole("Admin", "System"));
    options.AddPolicy(IdentityData.Customer, policy => policy.RequireRole("Customer", "System"));
    options.AddPolicy(IdentityData.Provider, policy => policy.RequireRole("Provider", "System"));
    options.AddPolicy(IdentityData.System, policy => policy.RequireRole("System"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();
app.UseStaticFiles();
app.UseRouting();

app.UseHttpsRedirection();

app.UseCors(x => x
          .AllowAnyMethod()
          .AllowAnyHeader()
          .SetIsOriginAllowed(origin => true) // allow any origin 
          .AllowCredentials());

//app.UseMiddleware<GlobalExceptionMiddleware>();

//app.UseMiddleware<JwtMiddleware>();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
