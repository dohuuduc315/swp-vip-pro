﻿using Domain.Entities;
using Domain.Enums;

namespace Applications.Repositories
{
    public interface IServiceCategoryRepo : IGenericRepo<ServiceCategory>
    {
        public Task<List<ServiceCategory?>> GetServiceCategoryByType(ServiceType type);
    }
}
