﻿using Applications.Commons;
using Domain.Entities;
using Domain.Enums;

namespace Applications.Repositories
{
    public interface IBirdServiceBookingRepo : IGenericRepo<BirdServiceBooking>
    {
        Task<List<BirdServiceBooking?>> GetByCustomerId(int id);
        Task<List<BirdServiceBooking?>> GetByProviderId(int id);
        public Task<BirdServiceBooking?> GetbookingInfo(int id);
        Task<List<BirdServiceBooking?>> GetBookingByStatus(BookingStatus status);
    }
}
