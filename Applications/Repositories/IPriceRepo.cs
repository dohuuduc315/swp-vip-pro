﻿using Applications.Commons;
using Domain.Entities;
using Domain.Enums;

namespace Applications.Repositories
{
    public interface IPriceRepo : IGenericRepo<Price>
    {
        public Task<List<Price>> Filter(Location location, decimal StartPrice, decimal EndPrice, double rating, string Category, string BirdServiceName);
    }
}
