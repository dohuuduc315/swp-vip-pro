﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface ICommentRepo : IGenericRepo<Comment>
    {
        Task<Pagination<Comment?>> GetByProviderId(int id, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<Comment?>> GetAllComment(int pageIndex = 0, int pageSize = 10);
        Task<Comment?> GetById(int id);
        double GetAvgRating(int id);
    }
}
