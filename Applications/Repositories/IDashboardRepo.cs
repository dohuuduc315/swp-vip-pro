﻿namespace Applications.Repositories
{
    public interface IDashboardRepo
    {
        int GetUserInMonth();
        int GetCustomerInMonth();
        int GetProviderInMonth();
        int GetOrderInMonth();
        int GetOrder();
        int GetCustomer();
        int GetProvider();
    }
}
