﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IUserRepo : IGenericRepo<User>
    {
        Task<User?> Login(string username, string password);
        public Task<User?> GetUserByEmail(string email);
        public Task<User?> GetUserByCode(string code);
        public Task<User?> GetUserInfo(int id);
        Task<Pagination<User?>> GetByName(string name, int pageIndex = 0, int pageSize = 10);
        Task<bool> ExistUsername(string username);
    }
}
