﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IProviderRepo : IGenericRepo<Provider>
    {
        Task<Provider?> GetProviderInfo(int id);
        Task<Pagination<Provider?>> GetAllProviderUnauthorized(int pageIndex = 0, int pageSize = 10);
        Task<Pagination<Provider?>> GetAllProvider(int pageIndex = 0, int pageSize = 10);
        Task<Provider?> GetProviderInfByProid(int id);
        int GetServiceCreated(int id);
        int GetServiceCreatedInMonth(int id);
        decimal GetPrice(int id);
        decimal GetPriceInMonth(int id);
        int GetBooking(int id);
        int GetBookingInMonth(int id);
        Task<Pagination<Provider?>> GetByname(string name, int pageIndex = 0, int pageSize = 10);
    }
}
