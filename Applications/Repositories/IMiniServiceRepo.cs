﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IMiniServiceRepo : IGenericRepo<MiniService>
    {
        Task<Pagination<MiniService?>> GetAllMiniService(int pageIndex = 0, int pageSize = 10);
    }
}
