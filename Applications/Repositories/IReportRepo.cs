﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IReportRepo : IGenericRepo<Report>
    {
        Task<Pagination<Report?>> GetAllReport(int pageIndex = 0, int pageSize = 10);
        Task<Report?> GetReportById(int id);
        Task<Pagination<Report?>> GetByUserId(int id, int pageIndex = 0, int pageSize = 10);
    }
}
