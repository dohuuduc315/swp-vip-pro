﻿using Applications.Commons;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Repositories
{
    public interface ICartRepo : IGenericRepo<Cart>
    {
        Task<Cart?> GetCartByUserId(int id);
        Task<Pagination<Cart?>> GetAllCart(int pageIndex = 0, int pageSize = 10);
    }
}
