﻿using Domain.Entities;

namespace Applications.Repositories
{
    public interface IBirdTypeRepo : IGenericRepo<BirdType>
    {
    }
}
