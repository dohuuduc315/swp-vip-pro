﻿using Applications.Commons;
using Domain.Entities;
using Domain.Enums;

namespace Applications.Repositories
{
    public interface ITransactionRepo : IGenericRepo<Transaction>
    {
        Task<Pagination<Transaction>> GetByTransactionStatus(TransactionStatus status, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<Transaction>> GetByTransactionByUserId(int UserId, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<Transaction?>> GetAllTransaction(int pageIndex = 0, int pageSize = 10);
        Task<Transaction?> GetById(int Id);
    }
}
