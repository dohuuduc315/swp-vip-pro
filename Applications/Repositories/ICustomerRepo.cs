﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface ICustomerRepo : IGenericRepo<Customer>
    {
        Task<Customer?> GetCustomerInfo(int id);
        Task<Pagination<Customer?>> GetAllCustomer(int pageIndex = 0, int pageSize = 10);
        Task<Customer?> GetCustomerInfByCusid(int id);
    }
}
