﻿using Domain.Entities;

namespace Applications.Repositories
{
    public interface IBirdServiceRepo : IGenericRepo<BirdService>
    {
        public Task<BirdService?> GetBirdServiceInfo(int id);
        public Task<List<BirdService?>> GetBirdServiceByProviderId(int id);
        public Task<List<BirdService>> GetAllBSV();
        Task<List<BirdService>> GetHighestBSV();
        Task<BirdService?> GetBirdServiceForBooking(int id);
    }
}
