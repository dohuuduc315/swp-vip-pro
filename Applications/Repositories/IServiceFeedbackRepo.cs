﻿using Domain.Entities;

namespace Applications.Repositories
{
    public interface IServiceFeedbackRepo : IGenericRepo<ServiceFeedback>
    {
        Task<List<ServiceFeedback?>> GetByBirdServiceId(int id);
        public double getAvgRating(int id);
    }
}
