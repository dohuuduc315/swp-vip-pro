﻿using Domain.Entities;

namespace Applications.Repositories
{
    public interface IWalletRepo : IGenericRepo<Wallet>
    {
        Task<Wallet?> GetByUserId(int id);
    }
}
