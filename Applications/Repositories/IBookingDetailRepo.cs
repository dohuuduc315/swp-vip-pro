﻿using Domain.Entities;

namespace Applications.Repositories
{
    public interface IBookingDetailRepo : IGenericRepo<BookingDetail>
    {
        Task<BookingDetail> GetById(int id);
    }
}
