﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface IBirdTypeService
    {
        Task<Response> GetById(int id);
        Task<Response> Create(BirdTypeViewModel model);
        Task<Response> GetAllBirdType(int pageIndex = 0, int pageSize = 10);
        Task<Response> UpdateBirdType(int id, BirdTypeViewModel model);
        Task<Response> DeleteBirdType(int id);
    }
}
