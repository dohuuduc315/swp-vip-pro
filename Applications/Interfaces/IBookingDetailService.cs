﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IBookingDetailService
    {
        Task<Response> GetBookingDetailByid(int id);
        Task<Response> UpdateBookingWorkingStatus(int id,WorkingStatus status);
    }
}