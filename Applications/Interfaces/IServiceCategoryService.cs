﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IServiceCategoryService
    {
        Task<Response> GetServiceCategoryInfor(int id);
        Task<Response> GetAllServiceCategory(int pageIndex = 0, int pageSize = 10);
        Task<Response> CreateServiceCategory(ServiceCategoryViewModel model);
        Task<Response> UpdateServiceCategory(int id, ServiceCategoryViewModel model);
        Task<Response> RemoveServiceCategory(int id);
        public Task<Response> GetServiceCategoryByType(ServiceType type);
    }
}
