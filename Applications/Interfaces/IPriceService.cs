﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IPriceService
    {
        Task<Response> GetPriceInfor(int id);
        Task<Response> GetAllPrice(int pageIndex = 0, int pageSize = 10);
        Task<Response> CreatePrice(Price model);
        Task<Response> UpdatePrice(int id, Price model);
        Task<Response> RemovePrice(int id);
        Task<Response> Filter(Location location, decimal StartPrice, decimal EndPrice, double rating, string Category, string BirdServiceName);
    }
}
