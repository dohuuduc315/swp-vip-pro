﻿using Applications.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface ICartService
    {
        Task<Response> GetAllCart(int pageIndex = 0, int pageSize = 10);
        Task<Response> GetCartByUserId(int id);
    }
}
