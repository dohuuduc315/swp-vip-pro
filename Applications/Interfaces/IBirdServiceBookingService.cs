﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Enums;

namespace Applications.Interfaces
{
    public interface IBirdServiceBookingService
    {
        Task<Response> GetByCustomerId(int id);
        Task<Response> GetByProviderId(int id);
        Task<Response> CreateBooking(CreateBookingViewModel model);
        Task<Response> GetBookingByid(int id);
        Task<Response> GetAllServiceBooking(int pageIndex = 0, int pageSize = 10);
        Task<Response> PayBooking(int id);
        Task<Response> GetBookingByStatus(BookingStatus status);
        Task<Response> CancelBooking(int id);
    }
}
