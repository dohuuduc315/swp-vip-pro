﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Entities;

namespace Applications.Interfaces
{
    public interface IBirdServiceService
    {
        Task<Response> GetBirdServiceById(int id);
        Task<Response> GetBirdServiceByProviderId(int id);
        Task<Response> CreateBirdService(CreateServiceViewModel model);
        Task<Response> UpdateBirdService(int id, UpdateServiceViewModel model);
        Task<Response> RemoveBirdService(int id);
        Task<Response> GetAllBirdService();
        Task<Response> GetHighestBSV();
    }
}
