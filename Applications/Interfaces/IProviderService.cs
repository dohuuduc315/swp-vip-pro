﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface IProviderService
    {
        Task<Response> GetProviderInfor(int id);
        Task<Response> GetAllProvider(int pageIndex = 0, int pageSize = 10);
        Task<Response> CreateProvider(ProviderViewModel model);
        Task<Response> UpdateProvider(int id, ProviderViewModel model);
        Task<Response> RemoveProvider(int id);
        Task<Response> GetAllProviderUnauthorized(int pageIndex = 0, int pageSize = 10);
        Task<Response> VerifyProvider(int id);
        Task<Response> BanProvider(int id);
        Task<Response> GetByProviderId(int id);
        Response GetServiceCreated(int id);
        Response GetServiceCreatedInMonth(int id);
        Response GetPrice(int id);
        Response GetPriceInMonth(int id);
        Response GetBooking(int id);
        Response GetBookingInMonth(int id);
        Task<Response> GetByName(string name, int pageIndex = 0, int pageSize = 10);
    }
}
