﻿using Applications.Commons;
using Applications.ViewModels;
using Domain.Enums;

namespace Applications.Interfaces
{
    public interface IReportService
    {
        Task<Response> GetReportInfor(int id);
        Task<Response> GetAllReport(int pageIndex = 0, int pageSize = 10);
        Task<Response> CreateReport(CreateReportViewModel model);
        Task<Response> UpdateReport(int id, UpdateReportViewModel model);
        Task<Response> RemoveReport(int id);
        Task<Response> GetReportByUserId(int id, int pageIndex = 0, int pageSize = 10);
        Task<Response> UpdateReportStatus(int id, ReportStatus status);
    }
}
