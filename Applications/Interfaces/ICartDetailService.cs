﻿using Applications.Commons;
using Applications.ViewModels;
using Applications.ViewModels.Cart;
using Applications.ViewModels.CartDetail;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface ICartDetailService
    {
        Task<Response> RemoveCartDetail(int id);
        Task<Response> CreateCartDetail(CreateCartDetailViewModel model);
        Task<Response> UpdateCartDetail(int id,UpdateCartDetailViewModel model);
    }
}
