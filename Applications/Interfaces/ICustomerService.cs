﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface ICustomerService
    {
        Task<Response> GetCustomerInfor(int id);
        Task<Response> GetAllCustomer(int pageIndex = 0, int pageSize = 10);
        Task<Response> CreateCustomer(CustomerViewModel model);
        Task<Response> UpdateCustomer(int id, CustomerViewModel model);
        Task<Response> RemoveCustomer(int id);
        Task<Response> GetByCustomerId(int id);
    }
}
