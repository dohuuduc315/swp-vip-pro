﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface IMiniServiceService
    {
        Task<Response> UpdateMiniService(int id, MiniServiceViewModel model);
        Task<Response> DeleteMiniService(int id);
        Task<Response> GetAllMiniService(int pageIndex = 0, int pageSize = 10);
    }
}
