﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface ICommentService
    {
        Task<Response> GetCommentInfor(int id);
        Task<Response> GetAllComment(int pageIndex = 0, int pageSize = 10);
        Task<Response> GetByProviderId(int id, int pageIndex = 0, int pageSize = 10);
        Task<Response> CreateComment(CommentViewModel model);
        Task<Response> UpdateComment(int id, CommentViewModel model);
        Task<Response> RemoveComment(int id);
    }
}
