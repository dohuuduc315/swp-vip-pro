﻿using Applications.Commons;

namespace Applications.Interfaces
{
    public interface IDashboardService
    {
        Response GetUserInMonth();
        Response GetCustomerInMonth();
        Response GetProviderInMonth();
        Response GetOrderInMonth();
        Response GetOrder();
        Response GetCustomer();
        Response GetProvider();
    }
}
