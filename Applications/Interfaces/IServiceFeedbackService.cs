﻿using Applications.Commons;
using Applications.ViewModels;

namespace Applications.Interfaces
{
    public interface IServiceFeedbackService
    {
        Task<Response> GetServiceFeedbackInfor(int id);
        Task<Response> GetAllServiceFeedback(int pageIndex = 0, int pageSize = 10);
        Task<Response> GetByBirdServiceId(int id);
        Task<Response> CreateServiceFeedback(ServiceFeedbackViewModel model);
        Task<Response> UpdateServiceFeedback(int id, ServiceFeedbackViewModel model);
        Task<Response> RemoveServiceFeedback(int id);
    }
}
