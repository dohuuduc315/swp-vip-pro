﻿using Applications.Commons;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Applications.Interfaces
{
    public interface IWalletService
    {
        Task<Response> GetByUserId(int id);
        Task<Response> UpdateBankToWallet(int id, string stk, Bank bank);
    }
}
