﻿using Domain.Enums;

namespace Applications.ViewModels
{
    public class ServiceCategoryViewModel
    {
        public string CategoryName { get; set; }
        public ServiceType ServiceType { get; set; }
    }

    public class GetServiceCategoryViewModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public ServiceType ServiceType { get; set; }
    }
}
