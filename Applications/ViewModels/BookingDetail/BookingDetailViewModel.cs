﻿using Applications.ViewModels.Service;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels
{
    public class BookingDetailViewModel
    {
        public int Id { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public WorkingStatus WorkingStatus { get; set; }
        public int BirdServiceBookingId { get; set; }
        public int BirdServiceId { get; set; }
        public ServiceViewModelForBooking BirdService { get; set; }
        public int? MiniServiceId { get; set; }
        public MiniServiceViewModel MiniService { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public int? PriceId { get; set; }
    }
}
