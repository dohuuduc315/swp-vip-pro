﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels
{
    public class UpdateMiniServiceViewModel
    {
        public int Id { get; set; }
        public string MiniServiceName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
