﻿using Domain.Enums;

namespace Applications.ViewModels
{
    public class GetWalletViewModel
    {
        public int Id { get; set; }
        public decimal WalletAmount { get; set; }
        public Bank? Bank { get; set; }
        public string? BankNumber { get; set; }
        public GetUserViewModel? User { get; set; }
    }
}
