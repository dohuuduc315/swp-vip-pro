﻿using Applications.ViewModels.Service;
using Domain.Entities;

namespace Applications.ViewModels.CartDetail
{
    public class CartDetailViewModel
    {
        public int Id { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string Description { get; set; }
        public ServiceViewModelForCart? BirdService { get; set; }
        public MiniServiceViewModel? MiniService { get; set; }
        public int Quantity { get; set; }
        public int CartId { get; set; }
        public decimal Price { get; set; }
        public int? PriceId { get; set; }
    }
}
