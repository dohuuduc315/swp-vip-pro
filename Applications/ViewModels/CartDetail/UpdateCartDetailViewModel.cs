﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels.Cart
{
    public class UpdateCartDetailViewModel
    {
        public int Id { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public string Description { get; set; }
        public int BirdServiceId { get; set; }
        public int? MiniServiceId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int? PriceId { get; set; }
    }
}
