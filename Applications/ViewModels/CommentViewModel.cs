﻿using Domain.Entities;

namespace Applications.ViewModels
{
    public class CommentViewModel
    {
        public double Rating { get; set; }
        public string CommentContent { get; set; }
        public int CustomerId { get; set; }
        public int ProviderId { get; set; }
    }

    public class GetCommentViewModel
    {
        public int Id { get; set; }
        public string CommentContent { get; set; }
        public double Rating { get; set; }
        public int CustomerId { get; set; }
        public int ProviderId { get; set; }
        public GetCustomerViewModel? Customer { get; set; }
        public GetProviderViewModel? Provider { get; set; }
    }
}
