﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Applications.ViewModels
{
    public class UserViewModel
    {
        public string AvatarURL { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string? Email { get; set; }
        public DateTime? DOB { get; set; }
        [Phone]
        public string? PhoneNumber { get; set; }
        public string? Gender { get; set; }
        public string? Image { get; set; }
    }

    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class GetUserViewModel
    {
        public int Id { get; set; }
        public string AvatarURL { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string? Email { get; set; }
        public DateTime? DOB { get; set; }
        [Phone]
        public string? PhoneNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? Gender { get; set; }
        public Status? Status { get; set; }
        public string? Image { get; set; }
        public Role? Role { get; set; }
        public GetUserCustomerViewModel? Customer { get; set; }
        public GetUserProviderViewModel? Provider { get; set; }
    }
}
