﻿using Domain.Entities;
using Domain.Enums;

namespace Applications.ViewModels
{
    public class ServiceViewModel
    {
        public int Id { get; set; }
        public string BirdServiceName { get; set; }
        public Status Status { get; set; }
        public string Description { get; set; }
        public Location? Location { get; set; }
        public string ServiceAvatarURL { get; set; }
        public double AvgRating { get; set; }
        public string VideoURL { get; set; }
        public string ImageURL { get; set; }
        public int ProviderId { get; set; }
        public ICollection<PriceViewModel>? Prices { get; set; }
        public ICollection<ServiceFeedbackViewModel>? ServiceFeedbacks { get; set; }
        public ICollection<MiniServiceViewModel>? MiniServices { get; set; }
        public GetServiceCategoryViewModel? ServiceCategory { get; set; }
    }
}
