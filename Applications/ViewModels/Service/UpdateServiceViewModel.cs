﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels
{
    public class UpdateServiceViewModel
    {
        public string BirdServiceName { get; set; }
        public Status Status { get; set; }
        public string Description { get; set; }
        public double AvgRating { get; set; }
        public string VideoURL { get; set; }
        public string ImageURL { get; set; }
        public Location? Location { get; set; }
        public int ServiceCategoryId { get; set; }
        public int ProviderId { get; set; }
        public ICollection<UpdatePriceViewModel>? Prices { get; set; }
        public ICollection<UpdateMiniServiceViewModel>? MiniServices { get; set; }
    }
}
