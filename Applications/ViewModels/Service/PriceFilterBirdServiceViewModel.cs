﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels
{
    public class PriceFilterBirdServiceViewModel
    {
        public string BirdServiceName { get; set; }
        public string VideoURL { get; set; }
        public string ImageURL { get; set; }
        public Status Status { get; set; }
        public string Description { get; set; }
        public Location? Location { get; set; }
        public int? ServiceCategoryId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public ServiceCategory? ServiceCategory { get; set; }
        public double AvgRating { get; set; }
        public ICollection<ServiceFeedback>? ServiceFeedbacks { get; set; }
        public ICollection<MiniService>? MiniServices { get; set; }
    }
}
