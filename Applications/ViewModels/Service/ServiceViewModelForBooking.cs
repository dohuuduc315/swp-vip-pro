﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels.Service
{
    public class ServiceViewModelForBooking
    {
        public int Id { get; set; }
        public string BirdServiceName { get; set; }
        public Status Status { get; set; }
        public string Description { get; set; }
        public Location? Location { get; set; }
        public string ServiceAvatarURL { get; set; }
        public double AvgRating { get; set; }
        public string VideoURL { get; set; }
        public string ImageURL { get; set; }
        public int ProviderId { get; set; }
        public ICollection<PriceViewModel>? Prices { get; set; }
        public ICollection<ServiceFeedbackViewModel>? ServiceFeedbacks { get; set; }
        public ICollection<MiniServiceViewModel>? MiniServices { get; set; }
        public GetServiceCategoryViewModel? ServiceCategory { get; set; }
    }
}
