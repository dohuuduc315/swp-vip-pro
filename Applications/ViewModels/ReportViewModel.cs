﻿using Domain.Entities;
using Domain.Enums;

namespace Applications.ViewModels
{
    public class ReportViewModel
    {
        public string ReportName { get; set; }
        public string Content { get; set; }
        public ReportStatus ReportStatus { get; set; }
        public string? AdminAnswer { get; set; }
        public int UserId { get; set; }
    }

    public class GetReportViewModel
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public string Content { get; set; }
        public ReportStatus ReportStatus { get; set; }
        public string? AdminAnswer { get; set; }
        public GetUserViewModel? User { get; set; }
    }

    public class CreateReportViewModel
    {
        public string ReportName { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
    }

    public class UpdateReportViewModel
    {
        public ReportStatus ReportStatus { get; set; }
        public string? AdminAnswer { get; set; }
    }
}
