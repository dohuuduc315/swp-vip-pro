﻿using Domain.Enums;

namespace Applications.ViewModels.Price
{
    public class AddPriceToBirdType
    {
        public string PriceName { get; set; }
        public ServiceType ServiceType { get; set; }
        public decimal PriceAmount { get; set; }
        public PriceType PriceType { get; set; }
    }
}
