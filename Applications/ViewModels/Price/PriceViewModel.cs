﻿using Domain.Enums;

namespace Applications.ViewModels
{
    public class PriceViewModel
    {
        public int Id { get; set; }
        public string PriceName { get; set; }
        public ServiceType ServiceType { get; set; }
        public decimal PriceAmount { get; set; }
        public PriceType PriceType { get; set; }
        public int BirdTypeId { get; set; }
        public BirdTypeViewModel BirdType { get; set; }
    }
}
