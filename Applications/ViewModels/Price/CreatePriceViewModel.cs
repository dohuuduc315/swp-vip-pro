﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels.Price
{
    public class CreatePriceViewModel
    {
        public string PriceName { get; set; }
        public ServiceType ServiceType { get; set; }
        public decimal PriceAmount { get; set; }
        public PriceType PriceType { get; set; }
        public int BirdTypeId { get; set; }
    }
}