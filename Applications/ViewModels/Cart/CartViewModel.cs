﻿using Applications.ViewModels.CartDetail;
using Domain.Entities;

namespace Applications.ViewModels.Cart
{
    public class CartViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public List<CartDetailViewModel>? CartDetails { get; set; }
    }
}
