﻿using Domain.Entities;

namespace Applications.ViewModels
{
    public class CustomerViewModel
    {
        public string CustomerName { get; set; }
        public UserViewModel? User { get; set; }
    }

    public class GetCustomerViewModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedAt { get; set; }
        public GetUserViewModel? User { get; set; }
    }

    public class GetUserCustomerViewModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
