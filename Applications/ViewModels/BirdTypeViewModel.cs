﻿using Applications.ViewModels.Price;
using Domain.Enums;

namespace Applications.ViewModels
{
    public class BirdTypeViewModel
    {
        public string BirdName { get; set; }
        public BirdSize BirdSize { get; set; }
    }

    public class GetBirdTypeViewModel
    {
        public int Id { get; set; }
        public string BirdName { get; set; }
        public BirdSize BirdSize { get; set; }
    }
}
