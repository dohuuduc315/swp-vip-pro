﻿namespace Applications.ViewModels
{
    public class MiniServiceViewModel
    {
        public int Id { get; set; }
        public string MiniServiceName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }

    public class CreateMiniServiceViewModel
    {
        public string MiniServiceName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
