﻿using Domain.Entities;
using Domain.Enums;

namespace Applications.ViewModels
{
    public class TransactionViewModel
    {
        public int Id { get; set; }
        public TransactionType TransactionType { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public decimal AmountTransaction { get; set; }
        public string? TransactionDescription { get; set; }
        public GetWalletViewModel? Wallet { get; set; }
    }

    public class CreateTransactionViewModel
    {
        public TransactionType TransactionType { get; set; }
        public DateTime? CreateAt { get; set; } = DateTime.UtcNow;
        public decimal AmountTransaction { get; set; }
        public string TransactionDescription { get; set; }
        public int WalletId { get; set; }
    }
}
