﻿namespace Applications.ViewModels
{
    public class ServiceFeedbackViewModel
    {
        public double Rating { get; set; }
        public string Content { get; set; }
        public int CustomerId { get; set; }
        public int BirdServiceId { get; set; }
    }

    public class GetServiceFeedbackViewModel
    {
        public int Id { get; set; }
        public double Rating { get; set; }
        public string Content { get; set; }
        public int CustomerId { get; set; }
        public int BirdServiceId { get; set; }
    }
}
