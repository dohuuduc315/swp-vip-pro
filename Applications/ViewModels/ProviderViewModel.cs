﻿namespace Applications.ViewModels
{
    public class ProviderViewModel
    {
        public string ProviderName { get; set; }
        public double? Rating { get; set; }
        public string? Description { get; set; }
        public UserViewModel? User { get; set; }
    }

    public class GetProviderViewModel
    {
        public int Id { get; set; }
        public string ProviderName { get; set; }
        public double? Rating { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public GetUserViewModel? User { get; set; }
    }

    public class GetUserProviderViewModel
    {
        public int Id { get; set; }
        public string ProviderName { get; set; }
        public double? Rating { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
