﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels
{
    public class UpdateBookingViewModel
    {
        public int CustomerId { get; set; }
        public int ProviderId { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public ICollection<CreateBookingDetailViewModel>? BookingDetails { get; set; }
    }
}
