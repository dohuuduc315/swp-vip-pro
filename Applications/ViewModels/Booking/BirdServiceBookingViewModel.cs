﻿using Domain.Enums;

namespace Applications.ViewModels
{
    public class BirdServiceBookingViewModel
    {
        public int Id { get; set; }
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public BookingStatus BookingStatus { get; set; }
        public int CustomerId { get; set; }
        public GetCustomerViewModel Customer { get; set; }
        public int ProviderId { get; set; }
        public GetProviderViewModel Provider { get; set; }
        public ICollection<BookingDetailViewModel>? BookingDetails { get; set; }
       public decimal TotalPrice { get; set; } 
    }
}
