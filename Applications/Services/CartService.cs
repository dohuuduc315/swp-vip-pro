﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using Applications.ViewModels.Cart;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CartService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> GetAllCart(int pageIndex = 0, int pageSize = 10)
        {
            var cartObj = await _unitOfWork.CartRepo.GetAllCart(pageIndex, pageSize);
            var listcart = cartObj.Items;
            var result = _mapper.Map<Pagination<CartViewModel>>(cartObj);
            if (cartObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NotFound, "No Cart Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetCartByUserId(int id)
        {
            var cartObj = await _unitOfWork.CartRepo.GetCartByUserId(id);
            if(cartObj == null)
            {
                return new Response(HttpStatusCode.NotFound, "No Cart Found");
            }
            foreach (var item in cartObj.CartDetails)
            {
                var birdserviceId = item.BirdServiceId;
                var Birdsvobj = await _unitOfWork.BirdServiceRepo.GetBirdServiceForBooking(birdserviceId);
                item.BirdService = Birdsvobj;
            }
            var result = _mapper.Map<CartViewModel>(cartObj);
            return new Response(HttpStatusCode.OK, "Success", result);
        }
    }
}
