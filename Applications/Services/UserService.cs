﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Utils;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly JWTSection _jwtSection;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;
        public UserService(IUnitOfWork unitOfWork,
            JWTSection jwtSection,
            IMapper mapper,
            IClaimsServices claimsServices)
        {
            _unitOfWork = unitOfWork;
            _jwtSection = jwtSection;
            _mapper = mapper;
            _claimsServices = claimsServices;

        }

        public async Task<Response> BanUser(int id)
        {
            var userObj = await _unitOfWork.UserRepo.GetEntityByIdAsync(id);
            if (userObj is not null)
            {
                userObj.Status = Domain.Enums.Status.InActive;
                _unitOfWork.UserRepo.UpdateAsync(userObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "User id not found");
        }

        public async Task<Response> CreateUser(UserViewModel userViewModel)
        {
            var user = _mapper.Map<User>(userViewModel);
            await _unitOfWork.UserRepo.CreateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllUser(int pageIndex = 0, int pageSize = 10)
        {
            var userObj = await _unitOfWork.UserRepo.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetUserViewModel>>(userObj);
            if (userObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No User Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetUserByName(string name, int pageIndex = 0, int pageSize = 10)
        {
            var userObj = await _unitOfWork.UserRepo.GetByName(name, pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetUserViewModel>>(userObj);
            if (userObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No User Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetUserInfor(int id)
        {
            var userObj = await _unitOfWork.UserRepo.GetUserInfo(id);
            var result = _mapper.Map<GetUserViewModel>(userObj);
            if (userObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No User Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> Login(LoginViewModel loginViewModel)
        {
            var user = await _unitOfWork.UserRepo.Login(loginViewModel.Username, loginViewModel.Password);
            if (user == null)
            {
                return new Response(HttpStatusCode.BadRequest, "Invalid username or password");
            }
            var tokenString = StringUtils.GenerateJwtToken(user.Id.ToString(), user.Username, user.Role.ToString(), user.Status.ToString(), _jwtSection);
            if (user.Status == Domain.Enums.Status.InActive)
            {
                return new Response(HttpStatusCode.BadRequest, "Your account has been blocked");
            }
            return new Response(HttpStatusCode.OK, "Success", tokenString);
        }

        public async Task<Response> RemoveUser(int id)
        {
            var userObj = await _unitOfWork.UserRepo.GetEntityByIdAsync(id);
            if (userObj is not null)
            {
                _unitOfWork.UserRepo.DeleteAsync(userObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> ResetPassword(ResetPasswordRequest request)
        {
            var user = await _unitOfWork.UserRepo.GetUserByCode(request.Code);

            if (user is null || user.ResetCodeExpires < DateTime.Now)
            {
                return new Response(HttpStatusCode.BadRequest, "Invalid code!");
            }

            if (string.CompareOrdinal(request.Password, request.ConfirmPassword) != 0)
            {
                return new Response(HttpStatusCode.BadRequest, "the password and confirm password does not match!");
            }
            user.Password = request.ConfirmPassword;
            user.CodeResetPassword = null;
            user.ResetCodeExpires = null;
            _unitOfWork.UserRepo.UpdateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Success");
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UnbanUser(int id)
        {
            var userObj = await _unitOfWork.UserRepo.GetEntityByIdAsync(id);
            if (userObj is not null)
            {
                userObj.Status = Domain.Enums.Status.Active;
                _unitOfWork.UserRepo.UpdateAsync(userObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "User id not found");
        }

        public async Task<Response> UpdateUser(int id, UserViewModel userViewModel)
        {
            var userObj = await _unitOfWork.UserRepo.GetEntityByIdAsync(id);
            if (userObj is not null)
            {
                _mapper.Map(userViewModel, userObj);
                _unitOfWork.UserRepo.UpdateAsync(userObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
