﻿using Applications.Commons;
using Applications.Interfaces;
using System.Net;

namespace Applications.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DashboardService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Response GetCustomer()
        {
            var result = _unitOfWork.DashboardRepo.GetCustomer();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetCustomerInMonth()
        {
            var result = _unitOfWork.DashboardRepo.GetCustomerInMonth();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetOrder()
        {
            var result = _unitOfWork.DashboardRepo.GetOrder();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetOrderInMonth()
        {
            var result = _unitOfWork.DashboardRepo.GetOrderInMonth();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetProvider()
        {
            var result = _unitOfWork.DashboardRepo.GetProvider();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetProviderInMonth()
        {
            var result = _unitOfWork.DashboardRepo.GetProviderInMonth();
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetUserInMonth()
        {
            var result = _unitOfWork.DashboardRepo.GetUserInMonth();
            return new Response(HttpStatusCode.OK, "Success", result);
        }
    }
}
