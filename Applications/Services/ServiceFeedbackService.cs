﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class ServiceFeedbackService : IServiceFeedbackService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ServiceFeedbackService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateServiceFeedback(ServiceFeedbackViewModel model)
        {
            var customerId = await _unitOfWork.CustomerRepo.GetEntityByIdAsync(model.CustomerId);
            if (customerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found customer id");
            }

            var birdServiceId = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(model.BirdServiceId);
            if (birdServiceId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found bird service id");
            }

            var serviceFeedbackObj = _mapper.Map<ServiceFeedback>(model);
            await _unitOfWork.ServiceFeedbackRepo.CreateAsync(serviceFeedbackObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllServiceFeedback(int pageIndex = 0, int pageSize = 10)
        {
            var serviceFeedbackObj = await _unitOfWork.ServiceFeedbackRepo.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetServiceFeedbackViewModel>>(serviceFeedbackObj);
            if (serviceFeedbackObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Feedback Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByBirdServiceId(int id)
        {
            var serviceFeedbackObj = await _unitOfWork.ServiceFeedbackRepo.GetByBirdServiceId(id);
            var result = _mapper.Map<GetServiceFeedbackViewModel>(serviceFeedbackObj);
            if (serviceFeedbackObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Feedback Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetServiceFeedbackInfor(int id)
        {
            var serviceFeedbackObj = await _unitOfWork.ServiceFeedbackRepo.GetEntityByIdAsync(id);
            var result = _mapper.Map<GetServiceFeedbackViewModel>(serviceFeedbackObj);
            if (serviceFeedbackObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Feedback Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveServiceFeedback(int id)
        {
            var serviceFeedbackObj = await _unitOfWork.ServiceFeedbackRepo.GetEntityByIdAsync(id);
            if (serviceFeedbackObj is not null)
            {
                _unitOfWork.ServiceFeedbackRepo.DeleteAsync(serviceFeedbackObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateServiceFeedback(int id, ServiceFeedbackViewModel model)
        {
            var serviceFeedbackObj = await _unitOfWork.ServiceFeedbackRepo.GetEntityByIdAsync(id);
            if (serviceFeedbackObj is not null)
            {
                var customerId = await _unitOfWork.CustomerRepo.GetEntityByIdAsync(model.CustomerId);
                if (customerId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found customer id");
                }

                var birdServiceId = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(model.BirdServiceId);
                if (birdServiceId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found bird service id");
                }
                _mapper.Map(model, serviceFeedbackObj);
                _unitOfWork.ServiceFeedbackRepo.UpdateAsync(serviceFeedbackObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
