﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class ProviderService : IProviderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProviderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> BanProvider(int id)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetEntityByIdAsync(id);
            if (providerObj is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found provider id");
            }
            providerObj.IsAuthorized = false;
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Success");
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> CreateProvider(ProviderViewModel model)
        {
            var isExist = await _unitOfWork.UserRepo.ExistUsername(model.User.Username);
            if (!isExist)
            {
                Cart cart = new Cart();
                cart.IsDeleted = false;
                Wallet walletObj = new Wallet();
                walletObj.IsDeleted = false;
                walletObj.WalletAmount = 0;
                var providerObj = _mapper.Map<Provider>(model);
                providerObj.User.Wallet = walletObj;
                providerObj.User.Cart = cart;
                providerObj.IsAuthorized = false;
                providerObj.CreatedAt = DateTime.Now;
                providerObj.User.CreatedAt = DateTime.Now;
                providerObj.User.Role = Domain.Enums.Role.Provider;
                providerObj.User.Status = Domain.Enums.Status.Active;
                await _unitOfWork.ProviderRepo.CreateAsync(providerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Create success");
                }
                return new Response(HttpStatusCode.BadRequest, "Create fail");
            }
            return new Response(HttpStatusCode.BadRequest, "Username existed!");
        }

        public async Task<Response> GetAllProvider(int pageIndex = 0, int pageSize = 10)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetAllProvider(pageIndex, pageSize);
            foreach (var provider in providerObj.Items)
            {
                var comment = await _unitOfWork.CommentRepo.GetByProviderId(provider.Id);
                if (comment.Items.Count() > 0)
                {
                    var avgRating = _unitOfWork.CommentRepo.GetAvgRating(provider.Id);
                    provider.Rating = avgRating;
                }
                else
                {
                    provider.Rating = 0;
                }
            }
            _unitOfWork.ProviderRepo.UpdateRangeAsync(providerObj.Items);
            await _unitOfWork.SaveChangeAsync();
            var result = _mapper.Map<Pagination<GetProviderViewModel>>(providerObj);
            if (providerObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Provider Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetAllProviderUnauthorized(int pageIndex = 0, int pageSize = 10)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetAllProviderUnauthorized(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetProviderViewModel>>(providerObj);
            if (providerObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Provider Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetBooking(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetBooking(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetBookingInMonth(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetBookingInMonth(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByName(string name, int pageIndex = 0, int pageSize = 10)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetByname(name);
            var result = _mapper.Map<Pagination<GetProviderViewModel>>(providerObj);
            if (providerObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Provider Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByProviderId(int id)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetProviderInfByProid(id);
            var comment = await _unitOfWork.CommentRepo.GetByProviderId(providerObj.Id);
            if (comment.Items.Count() > 0)
            {
                var avgRating = _unitOfWork.CommentRepo.GetAvgRating(providerObj.Id);
                providerObj.Rating = avgRating;
            }
            else
            {
                providerObj.Rating = 0;
            }
            _unitOfWork.ProviderRepo.UpdateAsync(providerObj);
            await _unitOfWork.SaveChangeAsync();
            var result = _mapper.Map<GetProviderViewModel>(providerObj);
            if (providerObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Provider Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetPrice(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetPrice(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetPriceInMonth(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetPriceInMonth(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetProviderInfor(int id)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetProviderInfo(id);
            var result = _mapper.Map<GetProviderViewModel>(providerObj);
            if (providerObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Provider Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetServiceCreated(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetServiceCreated(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public Response GetServiceCreatedInMonth(int id)
        {
            var result = _unitOfWork.ProviderRepo.GetServiceCreatedInMonth(id);
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveProvider(int id)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetProviderInfo(id);
            if (providerObj is not null)
            {
                _unitOfWork.UserRepo.DeleteAsync(providerObj.User);
                _unitOfWork.ProviderRepo.DeleteAsync(providerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found user id");
        }

        public async Task<Response> UpdateProvider(int id, ProviderViewModel model)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetProviderInfByProid(id);
            if (providerObj is not null)
            {
                _mapper.Map(model, providerObj);
                _unitOfWork.ProviderRepo.UpdateAsync(providerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found user id");

        }

        public async Task<Response> VerifyProvider(int id)
        {
            var providerObj = await _unitOfWork.ProviderRepo.GetProviderInfo(id);
            if (providerObj is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found provider id");
            }
            providerObj.IsAuthorized = true;
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Success");
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
