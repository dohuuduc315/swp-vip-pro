﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Applications.Services
{
    public class ServiceCategoryService : IServiceCategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ServiceCategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateServiceCategory(ServiceCategoryViewModel model)
        {
            var categoryObj = _mapper.Map<ServiceCategory>(model);
            await _unitOfWork.ServiceCategoryRepo.CreateAsync(categoryObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllServiceCategory(int pageIndex = 0, int pageSize = 10)
        {
            var categoryObj = await _unitOfWork.ServiceCategoryRepo.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetServiceCategoryViewModel>>(categoryObj);
            if (categoryObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Category Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetServiceCategoryByType(ServiceType type)
        {
            var categoryObj = await _unitOfWork.ServiceCategoryRepo.GetServiceCategoryByType(type);
            var result = _mapper.Map<List<GetServiceCategoryViewModel>>(categoryObj);
            if (categoryObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Category Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetServiceCategoryInfor(int id)
        {
            var categoryObj = await _unitOfWork.ServiceCategoryRepo.GetEntityByIdAsync(id);
            var result = _mapper.Map<GetServiceCategoryViewModel>(categoryObj);
            if (categoryObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Category Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveServiceCategory(int id)
        {
            var categoryObj = await _unitOfWork.ServiceCategoryRepo.GetEntityByIdAsync(id);
            if (categoryObj is not null)
            {
                _unitOfWork.ServiceCategoryRepo.DeleteAsync(categoryObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateServiceCategory(int id, ServiceCategoryViewModel model)
        {
            var categoryObj = await _unitOfWork.ServiceCategoryRepo.GetEntityByIdAsync(id);
            if (categoryObj is not null)
            {
                _mapper.Map(model, categoryObj);
                _unitOfWork.ServiceCategoryRepo.UpdateAsync(categoryObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
