﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class BirdServiceService : IBirdServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BirdServiceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateBirdService(CreateServiceViewModel model)
        {
            var serviceCategoryId = await _unitOfWork.ServiceCategoryRepo.GetEntityByIdAsync(model.ServiceCategoryId);
            if (serviceCategoryId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found service category id");
            }

            var providerId = await _unitOfWork.ProviderRepo.GetEntityByIdAsync(model.ProviderId);
            if (providerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found provider id");
            }

            foreach (var item in model.Prices)
            {
                var birdTypeId = await _unitOfWork.BirdTypeRepo.GetEntityByIdAsync(item.BirdTypeId);
                if (birdTypeId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found bird type id");
                }
            }

            var birdserviceObj = _mapper.Map<BirdService>(model);
            birdserviceObj.CreatedAt = DateTime.Now;
            foreach(var item in birdserviceObj.MiniServices)
            {
                item.CreatedAt = DateTime.Now;
            }
            await _unitOfWork.BirdServiceRepo.CreateAsync(birdserviceObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllBirdService()
        {
            var serviceObj = await _unitOfWork.BirdServiceRepo.GetAllBSV();
            var result = _mapper.Map<List<ServiceViewModel>>(serviceObj);
            if (serviceObj.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetBirdServiceById(int id)
        {
            var birdserviceObj = await _unitOfWork.BirdServiceRepo.GetBirdServiceInfo(id);
            var test = await _unitOfWork.ServiceFeedbackRepo.GetByBirdServiceId(birdserviceObj.Id);
            if (test.Count() > 0)
            {
                var avgRating = _unitOfWork.ServiceFeedbackRepo.getAvgRating(birdserviceObj.Id);
                birdserviceObj.AvgRating = avgRating;
            }
            else
            {
                birdserviceObj.AvgRating = 0;
            }
            _unitOfWork.BirdServiceRepo.UpdateAsync(birdserviceObj);
            await _unitOfWork.SaveChangeAsync();
            var result = _mapper.Map<ServiceViewModel>(birdserviceObj);
            if (birdserviceObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetBirdServiceByProviderId(int id)
        {
            var birdserviceObj = await _unitOfWork.BirdServiceRepo.GetBirdServiceByProviderId(id);
            
            foreach (var item in birdserviceObj)
            {
                var test = await _unitOfWork.ServiceFeedbackRepo.GetByBirdServiceId(item.Id);
                if (test.Count() > 0)
                {
                    var avgRating = _unitOfWork.ServiceFeedbackRepo.getAvgRating(item.Id);
                    item.AvgRating = avgRating;
                }
                else
                {
                    item.AvgRating = 0;
                }
            }
            _unitOfWork.BirdServiceRepo.UpdateRangeAsync(birdserviceObj);
            var result = _mapper.Map<List<ServiceViewModel>>(birdserviceObj);
            await _unitOfWork.SaveChangeAsync();
            if (birdserviceObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetHighestBSV()
        {
            var serviceObj = await _unitOfWork.BirdServiceRepo.GetHighestBSV();
            var result = _mapper.Map<List<ServiceViewModel>>(serviceObj);
            if (serviceObj.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Found", result);
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveBirdService(int id)
        {
            var birdserviceObj = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(id);
            if (birdserviceObj is not null)
            {
                _unitOfWork.BirdServiceRepo.DeleteAsync(birdserviceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateBirdService(int id, UpdateServiceViewModel model)
        {
            var birdserviceObj = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(id);
            if (birdserviceObj is not null)
            {
                _mapper.Map(model, birdserviceObj);
                _unitOfWork.BirdServiceRepo.UpdateAsync(birdserviceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
