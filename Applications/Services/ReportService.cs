﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Applications.Services
{
    public class ReportService : IReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReportService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateReport(CreateReportViewModel model)
        {
            var userId = await _unitOfWork.UserRepo.GetEntityByIdAsync(model.UserId);
            if (userId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found user id");
            }
            var reportObj = _mapper.Map<Report>(model);
            reportObj.ReportStatus = Domain.Enums.ReportStatus.Pending;
            await _unitOfWork.ReportRepo.CreateAsync(reportObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllReport(int pageIndex = 0, int pageSize = 10)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetAllReport(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetReportViewModel>>(reportObj);
            if (reportObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Report Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetReportByUserId(int id, int pageIndex = 0, int pageSize = 10)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetByUserId(id);
            var result = _mapper.Map<Pagination<GetReportViewModel>>(reportObj);
            if (reportObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Report Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetReportInfor(int id)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetReportById(id);
            var result = _mapper.Map<GetReportViewModel>(reportObj);
            if (reportObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Report Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveReport(int id)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetEntityByIdAsync(id);
            if (reportObj is not null)
            {
                _unitOfWork.ReportRepo.DeleteAsync(reportObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateReport(int id, UpdateReportViewModel model)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetEntityByIdAsync(id);
            if (reportObj is not null)
            {
                _mapper.Map(model, reportObj);
                _unitOfWork.ReportRepo.UpdateAsync(reportObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found report");
        }

        public async Task<Response> UpdateReportStatus(int id, ReportStatus status)
        {
            var reportObj = await _unitOfWork.ReportRepo.GetEntityByIdAsync(id);
            if (reportObj is not null)
            {
                reportObj.ReportStatus = status;
                _unitOfWork.ReportRepo.UpdateAsync(reportObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found report");
        }
    }
}
