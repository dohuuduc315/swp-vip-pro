﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateComment(CommentViewModel model)
        {
            var customerId = await _unitOfWork.CustomerRepo.GetEntityByIdAsync(model.CustomerId);
            if (customerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found customer id");
            }

            var providerId = await _unitOfWork.ProviderRepo.GetEntityByIdAsync(model.ProviderId);
            if (providerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found provider id");
            }

            var commentObj = _mapper.Map<Comment>(model);
            await _unitOfWork.CommentRepo.CreateAsync(commentObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetAllComment(int pageIndex = 0, int pageSize = 10)
        {
            var commentObj = await _unitOfWork.CommentRepo.GetAllComment(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetCommentViewModel>>(commentObj);
            if (commentObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Comment Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByProviderId(int id, int pageIndex = 0, int pageSize = 10)
        {
            var commentObj = await _unitOfWork.CommentRepo.GetByProviderId(id);
            var result = _mapper.Map<Pagination<GetCommentViewModel>>(commentObj);
            if (commentObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Comment Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetCommentInfor(int id)
        {
            var commentObj = await _unitOfWork.CommentRepo.GetById(id);
            var result = _mapper.Map<GetCommentViewModel>(commentObj);
            if (commentObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Comment Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveComment(int id)
        {
            var commentObj = await _unitOfWork.CommentRepo.GetEntityByIdAsync(id);
            if (commentObj is not null)
            {
                _unitOfWork.CommentRepo.DeleteAsync(commentObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateComment(int id, CommentViewModel model)
        {
            var commentObj = await _unitOfWork.CommentRepo.GetEntityByIdAsync(id);
            if (commentObj is not null)
            {
                var customerId = await _unitOfWork.CustomerRepo.GetEntityByIdAsync(model.CustomerId);
                if (customerId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found customer id");
                }

                var providerId = await _unitOfWork.ProviderRepo.GetEntityByIdAsync(model.ProviderId);
                if (providerId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found provider id");
                }
                _mapper.Map(model, commentObj);
                _unitOfWork.CommentRepo.UpdateAsync(commentObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
