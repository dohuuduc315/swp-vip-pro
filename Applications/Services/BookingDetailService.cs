﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class BookingDetailService : IBookingDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookingDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> GetBookingDetailByid(int id)
        {
            var bookingdetailobj = await _unitOfWork.BookingDetailRepo.GetEntityByIdAsync(id);
            var result = _mapper.Map<BookingDetailViewModel>(bookingdetailobj);
            if (bookingdetailobj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> UpdateBookingWorkingStatus(int id,WorkingStatus status)
        {
            var bookingdetailobj = await _unitOfWork.BookingDetailRepo.GetById(id);
            if (bookingdetailobj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No booking detail Found");
            }
            if(status == Domain.Enums.WorkingStatus.CheckedIn)
            {
                if(bookingdetailobj.ServiceStartDate.Date > DateTime.Today.Date)
                {
                    return new Response(HttpStatusCode.BadRequest, "Cant checkin before start date");
                }
                if (bookingdetailobj.ServiceEndDate < DateTime.Today.Date)
                {
                    return new Response(HttpStatusCode.BadRequest, "Cant checkin after end date");
                }
                bookingdetailobj.ActualStartDate = DateTime.UtcNow;
                bookingdetailobj.WorkingStatus = status;
                bookingdetailobj.BirdServiceBooking.UpdateAt = DateTime.UtcNow;
                _unitOfWork.BirdServiceBookingRepo.UpdateAsync(bookingdetailobj.BirdServiceBooking);
                await _unitOfWork.SaveChangeAsync();
                _unitOfWork.BookingDetailRepo.UpdateAsync(bookingdetailobj);
                await _unitOfWork.SaveChangeAsync();
                return new Response(HttpStatusCode.OK, "Checked-in success");
            }
            else if(status == Domain.Enums.WorkingStatus.CheckedOut)
                {
                    if (bookingdetailobj.ServiceStartDate > DateTime.UtcNow)
                    {
                        return new Response(HttpStatusCode.BadRequest, "Cant checkout before start date");
                    }
                    if (bookingdetailobj.ServiceEndDate < DateTime.UtcNow)
                    {
                        return new Response(HttpStatusCode.BadRequest, "Cant checkout after end date");
                    }
                    bookingdetailobj.ActualEndDate = DateTime.UtcNow;
                    bookingdetailobj.WorkingStatus = status;
                    bookingdetailobj.BirdServiceBooking.UpdateAt = DateTime.UtcNow;
                _unitOfWork.BirdServiceBookingRepo.UpdateAsync(bookingdetailobj.BirdServiceBooking);
                await _unitOfWork.SaveChangeAsync();
                _unitOfWork.BookingDetailRepo.UpdateAsync(bookingdetailobj);
                await _unitOfWork.SaveChangeAsync();
                return new Response(HttpStatusCode.OK, "Checked-out success");

            }
            return new Response(HttpStatusCode.BadRequest, "Invalid Status input");
        }
    }
}
