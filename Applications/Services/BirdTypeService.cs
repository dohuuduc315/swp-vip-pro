﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;
using System.Reflection;

namespace Applications.Services
{
    public class BirdTypeService : IBirdTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BirdTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> Create(BirdTypeViewModel model)
        {
            var birdTypeObj = _mapper.Map<BirdType>(model);
            await _unitOfWork.BirdTypeRepo.CreateAsync(birdTypeObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> DeleteBirdType(int id)
        {
            var birdTypeObj = await _unitOfWork.BirdTypeRepo.GetEntityByIdAsync(id);
            if (birdTypeObj is not null)
            {
                _unitOfWork.BirdTypeRepo.DeleteAsync(birdTypeObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> GetAllBirdType(int pageIndex = 0, int pageSize = 10)
        {
            var birdTypeObj = await _unitOfWork.BirdTypeRepo.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetBirdTypeViewModel>>(birdTypeObj);
            if (birdTypeObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Type Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetById(int id)
        {
            var birdTypeObj = await _unitOfWork.BirdTypeRepo.GetEntityByIdAsync(id);
            var result = _mapper.Map<GetBirdTypeViewModel>(birdTypeObj);
            if (birdTypeObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Type Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> UpdateBirdType(int id, BirdTypeViewModel model)
        {
            var birdTypeObj = await _unitOfWork.BirdTypeRepo.GetEntityByIdAsync(id);
            if (birdTypeObj is not null)
            {
                _mapper.Map(model, birdTypeObj);
                _unitOfWork.BirdTypeRepo.UpdateAsync(birdTypeObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
