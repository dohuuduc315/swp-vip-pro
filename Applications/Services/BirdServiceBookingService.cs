﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Applications.Services
{
    public class BirdServiceBookingService : IBirdServiceBookingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BirdServiceBookingService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateBooking(CreateBookingViewModel model)
        {
            decimal BKprice = 0;
            int totalday = 0;
            decimal MNprice = 0;
            var providerId = await _unitOfWork.ProviderRepo.GetEntityByIdAsync(model.ProviderId);
            if (providerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found provider id");
            }

            var customerId = await _unitOfWork.CustomerRepo.GetEntityByIdAsync(model.CustomerId);
            if (customerId is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found customer id");
            }

            foreach (var item in model.BookingDetails)
            {
                if (item.ServiceEndDate != null && item.ServiceEndDate <= item.ServiceStartDate)
                {
                    return new Response(HttpStatusCode.BadRequest, "StartDate cant bigger than EndDate");
                }
                var birdservice = await _unitOfWork.BirdServiceRepo.GetBirdServiceInfo(item.BirdServiceId);
                if (birdservice is not null)
                {
                    if (item.MiniServiceId == 0)
                    {
                        item.MiniServiceId = null;
                    }
                    BKprice = BKprice + item.Price;
                }
                else
                {
                    return new Response(HttpStatusCode.NotFound, "Not found bird service id");
                }
            }

            var booking = _mapper.Map<BirdServiceBooking>(model);
            booking.CreateAt = DateTime.Now;
            booking.TotalPrice = BKprice;
            foreach (var item in booking.BookingDetails)
            {
                item.WorkingStatus = Domain.Enums.WorkingStatus.Waiting;
            }
            booking.BookingStatus = Domain.Enums.BookingStatus.Waiting;
            await _unitOfWork.BirdServiceBookingRepo.CreateAsync(booking);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> GetBookingByid(int id)
        {
            var booking = await _unitOfWork.BirdServiceBookingRepo.GetbookingInfo(id);
            if (booking.BookingDetails.Count > 0)
            {
                foreach (var item in booking.BookingDetails)
                {
                    var bsv = await _unitOfWork.BirdServiceRepo.GetBirdServiceForBooking(item.BirdServiceId);
                    if (item.MiniServiceId != null)
                    {
                        var minisv = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(id);
                        item.MiniService = minisv;
                    }
                    item.BirdService = bsv;
                }
            }
            var result = _mapper.Map<BirdServiceBookingViewModel>(booking);
            if (booking is null)
            {
                return new Response(HttpStatusCode.NoContent, "No booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByCustomerId(int id)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.GetByCustomerId(id);
            foreach (var booking in birdservicebookingObj)
            {
                if (booking.BookingDetails.Count > 0)
                {
                    foreach (var item in booking.BookingDetails)
                    {
                        var bsv = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(item.BirdServiceId);
                        if (item.MiniServiceId != null)
                        {
                            int miniId = int.Parse(item.MiniServiceId.ToString());
                            var minisv = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(miniId);
                            item.MiniService = minisv;
                        }
                        item.BirdService = bsv;
                    }
                }
            }
            var result = _mapper.Map<List<BirdServiceBookingViewModel>>(birdservicebookingObj);
            if (birdservicebookingObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }
        public async Task<Response> GetByProviderId(int id)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.GetByProviderId(id);
            foreach (var booking in birdservicebookingObj)
            {
                if(booking.BookingDetails.Count > 0)
                {
                    foreach (var item in booking.BookingDetails)
                    {
                        var bsv = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(item.BirdServiceId);
                        if (item.MiniServiceId != null)
                        {
                            int miniId = int.Parse(item.MiniServiceId.ToString());
                            var minisv = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(miniId);
                            item.MiniService = minisv;
                        }
                        item.BirdService = bsv;
                    }
                }
            }
            var result = _mapper.Map<List<BirdServiceBookingViewModel>>(birdservicebookingObj);
            if (birdservicebookingObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetAllServiceBooking(int pageIndex = 0, int pageSize = 10)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.ToPagination(pageIndex, pageSize);
            foreach (var booking in birdservicebookingObj.Items)
            {
                if (booking.BookingDetails != null)
                {
                    foreach (var item in booking.BookingDetails)
                    {
                        var bsv = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(item.BirdServiceId);
                        if (item.MiniService != null)
                        {
                            int miniId = int.Parse(item.MiniServiceId.ToString());
                            var minisv = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(miniId);
                            item.MiniService = minisv;
                        }
                        item.BirdService = bsv;
                    }
                }

            }
            var result = _mapper.Map<Pagination<BirdServiceBookingViewModel>>(birdservicebookingObj);
            if (birdservicebookingObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> PayBooking(int id)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.GetbookingInfo(id);
            if (birdservicebookingObj is not null)
            {
                var Customer = await _unitOfWork.CustomerRepo.GetCustomerInfByCusid(birdservicebookingObj.CustomerId);
                var Provider = await _unitOfWork.ProviderRepo.GetProviderInfByProid(birdservicebookingObj.ProviderId);
                if (Customer is null || Provider is null)
                {
                    return new Response(HttpStatusCode.BadRequest, "No customer/provider found");
                }
                var userCustomer = Customer.User;
                var userProvider = Provider.User;
                int cusWalletId = int.Parse(userCustomer.WalletId.ToString());
                int proWalletId = int.Parse(userProvider.WalletId.ToString());
                var cusWallet = await _unitOfWork.WalletRepo.GetEntityByIdAsync(cusWalletId);
                var proWallet = await _unitOfWork.WalletRepo.GetEntityByIdAsync(proWalletId);
                if (cusWallet.WalletAmount < birdservicebookingObj.TotalPrice)
                {
                    return new Response(HttpStatusCode.BadRequest, "Your wallet dont have enough money to pay for this Order");
                }
                else
                {
                    birdservicebookingObj.BookingStatus = Domain.Enums.BookingStatus.AlreadyPaid;
                    foreach (var item in birdservicebookingObj.BookingDetails)
                    {
                        item.WorkingStatus = Domain.Enums.WorkingStatus.Confirmed;
                    }
                    _unitOfWork.BirdServiceBookingRepo.UpdateAsync(birdservicebookingObj);
                    var isSuccess = await _unitOfWork.SaveChangeAsync();
                    cusWallet.WalletAmount = cusWallet.WalletAmount - birdservicebookingObj.TotalPrice;
                    proWallet.WalletAmount = proWallet.WalletAmount + birdservicebookingObj.TotalPrice;
                    _unitOfWork.WalletRepo.UpdateAsync(cusWallet);
                    _unitOfWork.WalletRepo.UpdateAsync(proWallet);
                    await _unitOfWork.SaveChangeAsync();
                    return new Response(HttpStatusCode.OK, "Pay succes");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "No order found");
        }

        public async Task<Response> GetBookingByStatus(BookingStatus status)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.GetBookingByStatus(status);
            var result = _mapper.Map<List<BirdServiceBookingViewModel>>(birdservicebookingObj);
            if (birdservicebookingObj.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Booking Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> CancelBooking(int id)
        {
            var birdservicebookingObj = await _unitOfWork.BirdServiceBookingRepo.GetbookingInfo(id);
            if (birdservicebookingObj == null)
            {
                return new Response(HttpStatusCode.NoContent, "No Booking Found");
            }
            else
            {
                birdservicebookingObj.BookingStatus = Domain.Enums.BookingStatus.Cancel;
                foreach (var item in birdservicebookingObj.BookingDetails)
                {
                    item.WorkingStatus = Domain.Enums.WorkingStatus.Cancel;
                }
                _unitOfWork.BirdServiceBookingRepo.UpdateAsync(birdservicebookingObj);
                await _unitOfWork.SaveChangeAsync();
                return new Response(HttpStatusCode.OK, "Cancel Booking Success");
            }
        }
    }
}
