﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels.Cart;
using Applications.ViewModels.CartDetail;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class CartDetailService : ICartDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CartDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateCartDetail(CreateCartDetailViewModel model)
        {
            var birdserviceobj = await _unitOfWork.BirdServiceRepo.GetEntityByIdAsync(model.BirdServiceId);
            if (birdserviceobj is null)
            {
                return new Response(HttpStatusCode.NotFound, "Not found Bird Service id");
            }
            if (model.MiniServiceId == 0)
            {
                model.MiniServiceId = null;
            }
            else
            {
                int miniServiceId = 0;
                miniServiceId = int.Parse(model.MiniServiceId.ToString());
                var miniserviceobj = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(miniServiceId);
                if (miniserviceobj is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found Miniserviceo id");
                }
            }
            var cartDetail = _mapper.Map<CartDetail>(model);
            await _unitOfWork.CartDetailRepo.CreateAsync(cartDetail);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> RemoveCartDetail(int id)
        {
            var cartdetailobj = await _unitOfWork.CartDetailRepo.GetEntityByIdAsync(id);
            if (cartdetailobj == null)
            {
                return new Response(HttpStatusCode.BadRequest, "No cart detail found");
            }
            _unitOfWork.CartDetailRepo.DeleteAsync(cartdetailobj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Success");
            }
            return new Response(HttpStatusCode.BadRequest, "Cant save data to server");
        }

        public async Task<Response> UpdateCartDetail(int id, UpdateCartDetailViewModel model)
        {
            var Cartdtobj = await _unitOfWork.CartDetailRepo.GetEntityByIdAsync(id);
            if (Cartdtobj is not null)
            {
                _mapper.Map(model, Cartdtobj);
                _unitOfWork.CartDetailRepo.UpdateAsync(Cartdtobj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
