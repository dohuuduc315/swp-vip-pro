﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Net;

namespace Applications.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;
        public TransactionService(IUnitOfWork unitOfWork,
            IMapper mapper,
            IClaimsServices claimsServices)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _claimsServices = claimsServices;
        }

        public async Task<Response> GetAllTransaction(int pageIndex = 0, int pageSize = 10)
        {
            var transactionObj = await _unitOfWork.TransactionRepo.GetAllTransaction(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transactionObj);
            if (!transactionObj.Items.Any())
            {
                return new Response(HttpStatusCode.NotFound, "No Transaction Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetTransactionInfor(int id)
        {
            var transactionObj = await _unitOfWork.TransactionRepo.GetById(id);
            var result = _mapper.Map<TransactionViewModel>(transactionObj);
            if (transactionObj is null)
            {
                return new Response(HttpStatusCode.NotFound, "No Transaction Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveTransaction(int id)
        {
            var transactionObj = await _unitOfWork.TransactionRepo.GetEntityByIdAsync(id);
            if (transactionObj is not null)
            {

                _unitOfWork.TransactionRepo.DeleteAsync(transactionObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdateTransaction(int id, TransactionViewModel transactionViewModel)
        {
            //validate data

            var transactionObj = await _unitOfWork.TransactionRepo.GetEntityByIdAsync(id);
            if (transactionObj is null)
                return new Response(HttpStatusCode.BadRequest, "Fail");

            _mapper.Map(transactionViewModel, transactionObj);
            _unitOfWork.TransactionRepo.UpdateAsync(transactionObj);
            await _unitOfWork.SaveChangeAsync();

            return new Response(HttpStatusCode.Accepted, "Success");
        }

        public async Task<Response> AuthorizeTransaction(int id, TransactionStatus status)
        {
            //validate data

            var transactionObj = await _unitOfWork.TransactionRepo.GetEntityByIdAsync(id);
            var Walletobj = await _unitOfWork.WalletRepo.GetEntityByIdAsync(transactionObj.WalletId);
            if (transactionObj is null)
                return new Response(HttpStatusCode.BadRequest, "Fail");
            if (status == Domain.Enums.TransactionStatus.Complete && transactionObj.TransactionType == Domain.Enums.TransactionType.withdraw)
            {
                transactionObj.TransactionStatus = Domain.Enums.TransactionStatus.Complete;
                Walletobj.WalletAmount = Walletobj.WalletAmount - transactionObj.AmountTransaction;
                if (Walletobj.WalletAmount < 0)
                {
                    return new Response(HttpStatusCode.BadRequest, "Wallet dont have enough amount to confirm withdraw");
                }
                _unitOfWork.WalletRepo.UpdateAsync(Walletobj);
                await _unitOfWork.SaveChangeAsync();
            }
            else if (status == Domain.Enums.TransactionStatus.Complete && transactionObj.TransactionType == Domain.Enums.TransactionType.recharge)
            {
                transactionObj.TransactionStatus = Domain.Enums.TransactionStatus.Complete;
                Walletobj.WalletAmount = Walletobj.WalletAmount + transactionObj.AmountTransaction;
                _unitOfWork.WalletRepo.UpdateAsync(Walletobj);
                await _unitOfWork.SaveChangeAsync();
            }
            else if (status == Domain.Enums.TransactionStatus.Failed)
            {
                transactionObj.TransactionStatus = Domain.Enums.TransactionStatus.Failed;
            }
            _unitOfWork.TransactionRepo.UpdateAsync(transactionObj);
            await _unitOfWork.SaveChangeAsync();

            return new Response(HttpStatusCode.Accepted, "Success");
        }

        public async Task<Response> CreateTransaction(CreateTransactionViewModel transactionViewModel)
        {
            var Walletobj = await _unitOfWork.WalletRepo.GetEntityByIdAsync(transactionViewModel.WalletId);
            if (transactionViewModel.TransactionType == Domain.Enums.TransactionType.withdraw && transactionViewModel.AmountTransaction >= Walletobj.WalletAmount)
            {
                return new Response(HttpStatusCode.BadRequest, "Your Wallet Amount Not Enough To Withdraw");
            }
            var transactionObj = _mapper.Map<Transaction>(transactionViewModel);
            transactionObj.TransactionStatus = Domain.Enums.TransactionStatus.WaitingForConFirm;
            await _unitOfWork.TransactionRepo.CreateAsync(transactionObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.Created, "Create success", transactionObj);
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }
        //remember to only put validation that is necessary
        private async Task<Response?> ValidateData(CreateTransactionViewModel transactionViewModel)
        {
            return null;
        }

        public async Task<Response> GetTransactionByStatus(TransactionStatus status, int pageIndex = 0, int pageSize = 10)
        {
            var transactionObj = await _unitOfWork.TransactionRepo.GetByTransactionStatus(status, pageIndex, pageSize);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transactionObj);
            if (!transactionObj.Items.Any())
            {
                return new Response(HttpStatusCode.NotFound, "No Transaction Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetTransactionByUserId(int id, int pageIndex = 0, int pageSize = 10)
        {
            var transactionObj = await _unitOfWork.TransactionRepo.GetByTransactionByUserId(id, pageIndex, pageSize);
            var result = _mapper.Map<Pagination<TransactionViewModel>>(transactionObj);
            if (!transactionObj.Items.Any())
            {
                return new Response(HttpStatusCode.NotFound, "No Transaction Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }
    }
}
