﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Enums;
using System.Net;

namespace Applications.Services
{
    public class WalletService : IWalletService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public WalletService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> GetByUserId(int id)
        {
            var wallet = await _unitOfWork.WalletRepo.GetByUserId(id);
            var result = _mapper.Map<GetWalletViewModel>(wallet);
            if (wallet is null)
            {
                return new Response(HttpStatusCode.NoContent, "No User Id Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> UpdateBankToWallet(int id, string stk, Bank bank)
        {
            var wallet = await _unitOfWork.WalletRepo.GetEntityByIdAsync(id);
            if (wallet == null)
            {
                return new Response(HttpStatusCode.NotFound, "No Wallet Found");
            }
            wallet.Bank = bank;
            wallet.BankNumber = stk;
            _unitOfWork.WalletRepo.UpdateAsync(wallet);
            await _unitOfWork.SaveChangeAsync();
            return new Response(HttpStatusCode.OK, "Update Bank success");
        }
    }
}
