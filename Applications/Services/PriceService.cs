﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class PriceService : IPriceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PriceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreatePrice(Price model)
        {
            var priceObj = _mapper.Map<Price>(model);
            await _unitOfWork.PriceRepo.CreateAsync(priceObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return new Response(HttpStatusCode.OK, "Create success");
            }
            return new Response(HttpStatusCode.BadRequest, "Create fail");
        }

        public async Task<Response> Filter(Location location, decimal StartPrice, decimal EndPrice, double rating, string Category, string BirdServiceName)
        {
            var price = await _unitOfWork.PriceRepo.Filter(location, StartPrice, EndPrice,rating,Category, BirdServiceName);
            var result = _mapper.Map<List<PriceFilterBirdServiceViewModel>>(price);
            if (price is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Service Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetAllPrice(int pageIndex = 0, int pageSize = 10)
        {
            var priceObj = await _unitOfWork.PriceRepo.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<PriceViewModel>>(priceObj);
            if (priceObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Bird Price Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetPriceInfor(int id)
        {
            var priceObj = await _unitOfWork.PriceRepo.GetEntityByIdAsync(id);
            var result = _mapper.Map<PriceViewModel>(priceObj);
            if (priceObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Price Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemovePrice(int id)
        {
            var priceObj = await _unitOfWork.PriceRepo.GetEntityByIdAsync(id);
            if (priceObj is not null)
            {
                _unitOfWork.PriceRepo.DeleteAsync(priceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> UpdatePrice(int id, Price model)
        {
            var priceObj = await _unitOfWork.PriceRepo.GetEntityByIdAsync(id);
            if (priceObj is not null)
            {
                var priceId = await _unitOfWork.PriceRepo.GetEntityByIdAsync(model.Id);
                if (priceId is null)
                {
                    return new Response(HttpStatusCode.NotFound, "Not found price id");
                }
                _mapper.Map(model, priceObj);
                _unitOfWork.PriceRepo.UpdateAsync(priceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
