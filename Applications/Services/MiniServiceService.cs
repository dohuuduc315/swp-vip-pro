﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using System.Net;

namespace Applications.Services
{
    public class MiniServiceService : IMiniServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MiniServiceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<Response> DeleteMiniService(int id)
        {
            var miniServiceObj = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(id);
            if (miniServiceObj is not null)
            {
                _unitOfWork.MiniServiceRepo.DeleteAsync(miniServiceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }

        public async Task<Response> GetAllMiniService(int pageIndex = 0, int pageSize = 10)
        {
            var providerObj = await _unitOfWork.MiniServiceRepo.GetAllMiniService(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<MiniServiceViewModel>>(providerObj);
            if (providerObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Service Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> UpdateMiniService(int id, MiniServiceViewModel model)
        {
            var miniServiceObj = await _unitOfWork.MiniServiceRepo.GetEntityByIdAsync(id);
            if (miniServiceObj is not null)
            {
                _mapper.Map(model, miniServiceObj);
                _unitOfWork.MiniServiceRepo.UpdateAsync(miniServiceObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
            }
            return new Response(HttpStatusCode.BadRequest, "Fail");
        }
    }
}
