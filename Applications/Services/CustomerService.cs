﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels;
using AutoMapper;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> CreateCustomer(CustomerViewModel model)
        {
            var isExist = await _unitOfWork.UserRepo.ExistUsername(model.User.Username);
            if (!isExist)
            {
                Cart cart = new Cart();
                cart.IsDeleted = false;
                Wallet walletObj = new Wallet();
                walletObj.IsDeleted = false;
                walletObj.WalletAmount = 0;
                var customerObj = _mapper.Map<Customer>(model);
                customerObj.User.Wallet = walletObj;
                customerObj.User.Cart = cart;
                customerObj.CreatedAt = DateTime.Now;
                customerObj.User.Role = Domain.Enums.Role.Customer;
                customerObj.User.Status = Domain.Enums.Status.Active;
                customerObj.User.CreatedAt = DateTime.Now;
                await _unitOfWork.CustomerRepo.CreateAsync(customerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Create success");
                }
                return new Response(HttpStatusCode.BadRequest, "Create fail");
            }
            return new Response(HttpStatusCode.BadRequest, "Username existed!");
        }

        public async Task<Response> GetAllCustomer(int pageIndex = 0, int pageSize = 10)
        {
            var customerObj = await _unitOfWork.CustomerRepo.GetAllCustomer(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<GetCustomerViewModel>>(customerObj);
            if (customerObj.Items.Count() < 1)
            {
                return new Response(HttpStatusCode.NoContent, "No Customer Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetByCustomerId(int id)
        {
            var customerObj = await _unitOfWork.CustomerRepo.GetCustomerInfByCusid(id);
            var result = _mapper.Map<GetCustomerViewModel>(customerObj);
            if (customerObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Customer Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> GetCustomerInfor(int id)
        {
            var customerObj = await _unitOfWork.CustomerRepo.GetCustomerInfo(id);
            var result = _mapper.Map<GetCustomerViewModel>(customerObj);
            if (customerObj is null)
            {
                return new Response(HttpStatusCode.NoContent, "No Customer Found");
            }
            return new Response(HttpStatusCode.OK, "Success", result);
        }

        public async Task<Response> RemoveCustomer(int id)
        {
            var customerObj = await _unitOfWork.CustomerRepo.GetCustomerInfo(id);
            if (customerObj is not null)
            {
                _unitOfWork.UserRepo.DeleteAsync(customerObj.User);
                _unitOfWork.CustomerRepo.DeleteAsync(customerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found user id");
        }

        public async Task<Response> UpdateCustomer(int id, CustomerViewModel model)
        {
            var customerObj = await _unitOfWork.CustomerRepo.GetCustomerInfo(id);
            if (customerObj is not null)
            {
                _mapper.Map(model, customerObj);
                _unitOfWork.CustomerRepo.UpdateAsync(customerObj);
                var isSuccess = await _unitOfWork.SaveChangeAsync();
                if (isSuccess > 0)
                {
                    return new Response(HttpStatusCode.OK, "Success");
                }
                return new Response(HttpStatusCode.BadRequest, "Fail");
            }
            return new Response(HttpStatusCode.NotFound, "Not found user id");
        }
    }
}
