﻿namespace Applications.Commons
{
    public class IdentityData
    {
        public const string Customer = "Customer";
        public const string Provider = "Provider";
        public const string Admin = "Admin";
        public const string Member = "Member";
        public const string System = "System";
        public const string Role = "role";
    }
}
