﻿using Applications.Repositories;

namespace Applications
{
    public interface IUnitOfWork
    {
        public Task<int> SaveChangeAsync();
        public IUserRepo UserRepo { get; }
        public ICustomerRepo CustomerRepo { get; }
        public IProviderRepo ProviderRepo { get; }
        public IReportRepo ReportRepo { get; }
        public ICommentRepo CommentRepo { get; }
        public IServiceFeedbackRepo ServiceFeedbackRepo { get; }
        public IBirdServiceRepo BirdServiceRepo { get; }
        public IBirdServiceBookingRepo BirdServiceBookingRepo { get; }
        public IBookingDetailRepo BookingDetailRepo { get; }
        public IServiceCategoryRepo ServiceCategoryRepo { get; }
        public IPriceRepo PriceRepo { get; }
        public IDashboardRepo DashboardRepo { get; }
        public IMiniServiceRepo MiniServiceRepo { get; }
        public ITransactionRepo TransactionRepo { get; }
        public IWalletRepo WalletRepo { get; }
        public ICartRepo CartRepo { get; }
        public ICartDetailRepo CartDetailRepo { get; }
        public IBirdTypeRepo BirdTypeRepo { get; }
    }
}
