﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CartRepo : GenericRepo<Cart>, ICartRepo
    {
        public CartRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }
        public async Task<Pagination<Cart?>> GetAllCart(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Include(x => x.CartDetails.Where(x => x.IsDeleted == false)).ThenInclude(x => x.MiniService)
                                    .ThenInclude(x => x.BirdService).ThenInclude(x => x.Prices.Where(x => x.IsDeleted == false))
                                    .Where(x => x.IsDeleted == false).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Cart>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Cart?> GetCartByUserId(int id)
        {
            return await _dbSet.Include(x => x.CartDetails.Where(x => x.IsDeleted == false))
                               .FirstOrDefaultAsync(x => x.UserId == id && x.IsDeleted == false);
        }
    }
}