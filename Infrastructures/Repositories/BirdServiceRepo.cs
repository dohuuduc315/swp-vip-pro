﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class BirdServiceRepo : GenericRepo<BirdService>, IBirdServiceRepo
    {
        public BirdServiceRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }
        public async Task<BirdService?> GetBirdServiceInfo(int id)
        {
            var birdService = await _dbSet.Where(x => x.IsDeleted == false)
                                            .Include(x => x.Prices.Where(x => x.IsDeleted == false))
                                             .ThenInclude(x => x.BirdType)
                                            .Include(x => x.ServiceFeedbacks.Where(x => x.IsDeleted == false))
                                            .Include(x => x.ServiceCategory)
                                            .Include(x => x.MiniServices.Where(x => x.IsDeleted == false))
                                            .FirstOrDefaultAsync(x => x.Id == id);
            return birdService;
        }

        public async Task<BirdService?> GetBirdServiceForBooking(int id)
        {
            var birdService = await _dbSet.Where(x => x.IsDeleted == false)
                                            .Include(x => x.Prices.Where(x => x.IsDeleted == false))
                                            .ThenInclude(x => x.BirdType)
                                            .Include(x => x.ServiceFeedbacks.Where(x => x.IsDeleted == false))
                                            .Include(x => x.ServiceCategory)
                                            .Include(x => x.MiniServices.Where(x => x.IsDeleted == false))
                                            .Include(x => x.Provider)
                                            .FirstOrDefaultAsync(x => x.Id == id);
            return birdService;
        }

        public async Task<List<BirdService?>> GetBirdServiceByProviderId(int id)
        {
            var birdService = await _dbSet.Where(x => x.ProviderId == id && x.IsDeleted == false)
                                                                        .Include(x => x.Prices.Where(x => x.IsDeleted == false))
                                                                        .ThenInclude(x => x.BirdType)
                                                                        .Include(x => x.MiniServices.Where(x => x.IsDeleted == false))
                                                                        .Include(x => x.ServiceFeedbacks.Where(x => x.IsDeleted == false))
                                                                        .Include(x => x.ServiceCategory)
                                                                        .ToListAsync();
            return birdService;
        }

        public async Task<List<BirdService>> GetAllBSV()
        {
            return await _dbSet.Where(x => x.IsDeleted == false).AsNoTracking()
                                                                .Include(x => x.Prices.Where(x => x.IsDeleted == false))
                                                                .ThenInclude(x => x.BirdType)
                                                                .Include(x => x.MiniServices.Where(x => x.IsDeleted == false))
                                                                .Include(x => x.ServiceFeedbacks.Where(x => x.IsDeleted == false))
                                                                .Include(x => x.ServiceCategory).ToListAsync();
        }

        public async Task<List<BirdService>> GetHighestBSV()
        {
            return await _dbSet.Where(x => x.IsDeleted == false).OrderByDescending(x => x.AvgRating).Take(4).AsNoTracking()
                                                                .Include(x => x.Prices.Where(x => x.IsDeleted == false))
                                                                .ThenInclude(x => x.BirdType)
                                                                .Include(x => x.MiniServices.Where(x => x.IsDeleted == false))
                                                                .Include(x => x.ServiceFeedbacks.Where(x => x.IsDeleted == false))
                                                                .Include(x => x.ServiceCategory).ToListAsync();
        }
    }
}
