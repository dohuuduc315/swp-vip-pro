﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ServiceCategoryRepo : GenericRepo<ServiceCategory>, IServiceCategoryRepo
    {
        public ServiceCategoryRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<List<ServiceCategory?>> GetServiceCategoryByType(ServiceType type)
        {
            var ServiceCategory = await _dbSet.Where(x => x.ServiceType == type && x.IsDeleted == false).ToListAsync();
            return ServiceCategory;
        }
    }
}
