﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class WalletRepo : GenericRepo<Wallet>, IWalletRepo
    {
        public WalletRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<Wallet?> GetByUserId(int id)
        {
            return await _dbSet.Include(x => x.User).FirstOrDefaultAsync(x => x.User.Id == id && x.IsDeleted == false);
        }
    }
}
