﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;

namespace Infrastructures.Repositories
{
    public class BirdTypeRepo : GenericRepo<BirdType>, IBirdTypeRepo
    {
        public BirdTypeRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }
    }
}
