﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UserRepo : GenericRepo<User>, IUserRepo
    {
        public UserRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public Task<bool> ExistUsername(string username)
        {
            return _dbSet.AnyAsync(x => x.Username == username);
        }

        public async Task<Pagination<User?>> GetByName(string name, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.Username.Contains(name) && x.IsDeleted == false)
                                    .Include(x => x.Provider)
                                    .Include(x => x.Customer)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<User>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<User?> GetUserByCode(string code)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.CodeResetPassword == code);
        }

        public async Task<User?> GetUserByEmail(string email)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Email == email && x.IsDeleted == false);
        }

        public async Task<User?> GetUserInfo(int id)
        {
            var user = await _dbSet.Include(x => x.Provider)
                                   .Include(x => x.Customer)
                                   .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return user;
        }

        public Task<User?> Login(string username, string password)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Username == username && x.Password == password && x.IsDeleted == false);
        }
    }
}
