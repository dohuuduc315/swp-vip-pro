﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class CustomerRepo : GenericRepo<Customer>, ICustomerRepo
    {
        public CustomerRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<Pagination<Customer?>> GetAllCustomer(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.IsDeleted == false)
                                    .Include(x => x.User)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Customer>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Customer?> GetCustomerInfo(int id)
        {
            var customer = await _dbSet.Include(x => x.User)
                                       .FirstOrDefaultAsync(x => x.User.Id == id && x.IsDeleted == false);
            return customer;
        }

        public async Task<Customer?> GetCustomerInfByCusid(int id)
        {
            var customer = await _dbSet.Include(x => x.User)
                                       .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return customer;
        }
    }
}
