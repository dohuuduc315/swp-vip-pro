﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CartDetailRepo : GenericRepo<CartDetail>, ICartDetailRepo
    {
        public CartDetailRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

    }
}