﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class TransactionRepo : GenericRepo<Transaction>, ITransactionRepo
    {
        public TransactionRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<Pagination<Transaction?>> GetAllTransaction(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Include(x => x.Wallet).ThenInclude(x => x.User).Where(x => x.IsDeleted == false).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Transaction>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Transaction?> GetById(int Id)
        {
            return await _dbSet.Include(x => x.Wallet).ThenInclude(x => x.User).FirstOrDefaultAsync(x => x.Id == Id);
        }

        public async Task<Pagination<Transaction>> GetByTransactionByUserId(int UserId, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.Wallet.User.Id == UserId && x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Include(x => x.Wallet).ThenInclude(x => x.User).Where(x => x.Wallet.User.Id == UserId && x.IsDeleted == false).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Transaction>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Pagination<Transaction>> GetByTransactionStatus(TransactionStatus status, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.TransactionStatus == status).CountAsync();
            var items = await _dbSet.Include(x => x.Wallet).ThenInclude(x => x.User).Where(x => x.TransactionStatus == status).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Transaction>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
