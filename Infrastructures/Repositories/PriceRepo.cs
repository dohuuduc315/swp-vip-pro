﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace Infrastructures.Repositories
{
    public class PriceRepo : GenericRepo<Price>, IPriceRepo
    {
        public PriceRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<List<Price>> Filter(Location location, decimal StartPrice, decimal EndPrice, double rating,string Category, string BirdServiceName)
        {
            if(StartPrice != null && EndPrice != null)
            {
                var price = await _dbSet.Where(x => x.PriceAmount >= StartPrice && 
                                                    x.IsDeleted == false &&
                                                    x.PriceAmount <= EndPrice &&
                                                    x.BirdService.Provider.Location == location && 
                                                    x.BirdService.AvgRating == rating &&
                                                    x.BirdService.ServiceCategory.CategoryName.Contains(Category) &&
                                                    x.BirdService.BirdServiceName.Contains(BirdServiceName))
                                                    .Include(x => x.BirdService).ToListAsync();
                return price;
            }
            else
            {
                var price = await _dbSet.Where(x => x.BirdService.Provider.Location == location &&
                                                    x.IsDeleted == false &&
                                                    x.BirdService.AvgRating == rating &&
                                                    x.BirdService.ServiceCategory.CategoryName.Contains(Category) &&
                                                    x.BirdService.BirdServiceName.Contains(BirdServiceName))
                                                    .Include(x => x.BirdService).ToListAsync();
                return price;
            }
        }
    }
}
