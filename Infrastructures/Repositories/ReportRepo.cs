﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ReportRepo : GenericRepo<Report>, IReportRepo
    {
        public ReportRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<Pagination<Report?>> GetAllReport(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Include(x => x.User.Customer)
                                    .Include(x => x.User.Provider)
                                    .Where(x => x.IsDeleted == false).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Report>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Pagination<Report?>> GetByUserId(int id, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false && x.UserId == id).CountAsync();
            var items = await _dbSet.Include(x => x.User.Customer)
                                    .Include(x => x.User.Provider)
                                    .Where(x => x.IsDeleted == false && x.UserId == id).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Report>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Report?> GetReportById(int id)
        {
            var report = await _dbSet.Include(x => x.User.Customer)
                                     .Include(x => x.User.Provider)
                                     .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return report;
        }
    }
}
