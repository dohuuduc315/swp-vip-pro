﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class BirdServiceBookingRepo : GenericRepo<BirdServiceBooking>, IBirdServiceBookingRepo
    {
        public BirdServiceBookingRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }


        public async Task<List<BirdServiceBooking?>> GetByCustomerId(int id)
        {
            var items = await _dbSet.Where(x => x.CustomerId == id && x.IsDeleted == false)
                                    .Include(x => x.BookingDetails)
                                    .ToListAsync();

            return items;
        }

        public async Task<List<BirdServiceBooking?>> GetByProviderId(int id)
        {
            var items = await _dbSet.Where(x => x.ProviderId == id && x.IsDeleted == false)
                                    .Include(x => x.BookingDetails)
                                    .ToListAsync();
            return items;
        }

        public async Task<List<BirdServiceBooking?>> GetBookingByStatus(BookingStatus status)
        {
            var items = await _dbSet.Where(x => x.BookingStatus == status && x.IsDeleted == false)
                                    .Include(x => x.BookingDetails)
                                    .ToListAsync();
            return items;
        }

        public async Task<BirdServiceBooking?> GetbookingInfo(int id)
        {
            var booking = await _dbSet.Where(x => x.IsDeleted == false).Include(x => x.Customer).ThenInclude(x => x.User).Include(x => x.Provider).ThenInclude(x => x.User).Include(x => x.BookingDetails)
                                            .FirstOrDefaultAsync(x => x.Id == id);
            return booking;
        }
    }
}
