﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ProviderRepo : GenericRepo<Provider>, IProviderRepo
    {
        private readonly AppDbContext _context;

        public ProviderRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
            _context = context;
        }

        public async Task<Pagination<Provider?>> GetAllProvider(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.IsDeleted == false)
                                    .Include(x => x.User)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Provider>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Pagination<Provider?>> GetAllProviderUnauthorized(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.IsAuthorized.Equals(false) && x.IsDeleted == false)
                                    .Include(x => x.User)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Provider>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Provider?> GetProviderInfo(int id)
        {
            var provider = await _dbSet.Where(x => x.IsDeleted == false).Include(x => x.User)
                                       .FirstOrDefaultAsync(x => x.User.Id == id);
            return provider;
        }

        public async Task<Provider?> GetProviderInfByProid(int id)
        {
            var customer = await _dbSet.Include(x => x.User)
                                       .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return customer;
        }

        public int GetServiceCreated(int id)
        {
            var service = _context.BirdServices.Count(x => x.ProviderId == id);
            return service;
        }

        public int GetServiceCreatedInMonth(int id)
        {
            var service = _context.BirdServices.Count(x => x.ProviderId == id && x.CreatedAt.Month == DateTime.Now.Month);
            return service;
        }

        public decimal GetPrice(int id)
        {
            var price = _context.Transactions.Where(x => x.Wallet.User.ProviderId == id).Sum(x => x.AmountTransaction);
            return price;
        }

        public decimal GetPriceInMonth(int id)
        {
            var price = _context.Transactions.Where(x => x.Wallet.User.ProviderId == id && x.CreateAt.Month == DateTime.Now.Month)
                                             .Sum(x => x.AmountTransaction);
            return price;
        }

        public int GetBooking(int id)
        {
            var order = _context.BirdServiceBookings.Count(x => x.ProviderId == id);
            return order;
        }

        public int GetBookingInMonth(int id)
        {
            var order = _context.BirdServiceBookings.Count(x => x.ProviderId == id && x.CreateAt.Month == DateTime.Now.Month);
            return order;
        }

        public async Task<Pagination<Provider?>> GetByname(string name, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.ProviderName.Contains(name) && x.IsDeleted == false)
                                    .Include(x => x.User)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Provider>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
