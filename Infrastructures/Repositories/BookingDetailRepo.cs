﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class BookingDetailRepo : GenericRepo<BookingDetail>, IBookingDetailRepo
    {
        public BookingDetailRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<BookingDetail> GetById(int id)
        {
            var cartobj = await _dbSet.Include(x => x.BirdServiceBooking).FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return cartobj;
        }
    }
}
