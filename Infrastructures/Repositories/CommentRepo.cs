﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class CommentRepo : GenericRepo<Comment>, ICommentRepo
    {
        public CommentRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public async Task<Pagination<Comment?>> GetAllComment(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Include(x => x.Customer).ThenInclude(x => x.User)
                                    .Include(x => x.Provider).ThenInclude(x => x.User)
                                    .Where(x => x.IsDeleted == false)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Comment>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public double GetAvgRating(int id)
        {
            return _dbSet.Where(x => x.ProviderId == id && x.IsDeleted == false).Average(x => x.Rating);
        }

        public async Task<Comment?> GetById(int id)
        {
            var comment = await _dbSet.Include(x => x.Customer).ThenInclude(x => x.User)
                                      .Include(x => x.Provider).ThenInclude(x => x.User)
                                      .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            return comment;
        }

        public async Task<Pagination<Comment?>> GetByProviderId(int id, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.Where(x => x.IsDeleted == false).CountAsync();
            var items = await _dbSet.Where(x => x.IsDeleted == false && x.ProviderId == id).Include(x => x.Customer).ThenInclude(x => x.User)
                                    .Include(x => x.Provider).ThenInclude(x => x.User).Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Comment>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
