﻿using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ServiceFeedbackRepo : GenericRepo<ServiceFeedback>, IServiceFeedbackRepo
    {
        public ServiceFeedbackRepo(AppDbContext context, ICurrentTimeService currentTime, IClaimsServices claimsServices) : base(context, currentTime, claimsServices)
        {
        }

        public double getAvgRating(int id)
        {
            return _dbSet.Where(x => x.BirdServiceId == id && x.IsDeleted == false).Average(x => x.Rating);
        }

        public async Task<List<ServiceFeedback?>> GetByBirdServiceId(int id)
        {
            var serviceFeedback = await _dbSet.Where(x => x.BirdServiceId == id && x.IsDeleted == false).ToListAsync();
            return serviceFeedback;
        }
    }
}
