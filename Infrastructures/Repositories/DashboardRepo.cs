﻿using Applications.Repositories;

namespace Infrastructures.Repositories
{
    public class DashboardRepo : IDashboardRepo
    {
        private readonly AppDbContext _context;

        public DashboardRepo(AppDbContext context)
        {
            _context = context;
        }
        public int GetCustomer()
        {
            var customer = _context.Customers.Count();
            return customer;
        }

        public int GetCustomerInMonth()
        {
            var customer = _context.Customers.Count(x => x.CreatedAt.Month == DateTime.Now.Month);
            return customer;
        }

        public int GetOrder()   
        {
            var order = _context.BirdServiceBookings.Count();
            return order;
        }

        public int GetOrderInMonth()
        {
            var order = _context.BirdServiceBookings.Count(x => x.CreateAt.Month == DateTime.Now.Month);
            return order;
        }

        public int GetProvider()
        {
            var provider = _context.Providers.Count();
            return provider;
        }

        public int GetProviderInMonth()
        {
            var provider = _context.Providers.Count(x => x.CreatedAt.Month == DateTime.Now.Month);
            return provider;
        }

        public int GetUserInMonth()
        {
            var user = _context.Users.Count(x => x.CreatedAt.Month == DateTime.Now.Month);
            return user;
        }
    }
}
