﻿using Applications;
using Applications.Interfaces;
using Applications.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICurrentTimeService, CurrentTimeService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IProviderService, ProviderService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IServiceFeedbackService, ServiceFeedbackService>();
            services.AddScoped<IPriceService, PriceService>();
            services.AddScoped<IBirdServiceService, BirdServiceService>();
            services.AddScoped<IMailService, MailService>();
            services.AddScoped<IBirdServiceBookingService, BirdServiceBookingService>();
            services.AddScoped<IDashboardService, DashboardService>();
            services.AddScoped<IMiniServiceService, MiniServiceService>();
            services.AddScoped<IServiceCategoryService, ServiceCategoryService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<ICartDetailService, CartDetailService>();
            services.AddScoped<IBookingDetailService, BookingDetailService>();
            services.AddScoped<IBirdTypeService, BirdTypeService>();

            return services;
        }
    }
}
