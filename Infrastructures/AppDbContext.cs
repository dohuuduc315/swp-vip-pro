﻿using Domain.Entities;
using Infrastructures.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<BirdService> BirdServices { get; set; }
        public DbSet<BirdServiceBooking> BirdServiceBookings { get; set; }
        public DbSet<BookingDetail> BookingDetails { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ServiceCategory> ServiceCategories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ServiceFeedback> ServiceFeedbacks { get; set; }
        public DbSet<MiniService> MiniServices { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<BirdType> BirdTypes { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartDetail> CartDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
