﻿using Applications.Commons;
using Applications.ViewModels;
using Applications.ViewModels.Cart;
using Applications.ViewModels.CartDetail;
using Applications.ViewModels.Price;
using Applications.ViewModels.Service;
using AutoMapper;
using Domain.Entities;

namespace Infrastructures.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            /* pagination */
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            CreateMap<User, UserViewModel>().ReverseMap();
            CreateMap<User, LoginViewModel>().ReverseMap();
            CreateMap<Customer, CustomerViewModel>().ReverseMap();
            CreateMap<Provider, ProviderViewModel>().ReverseMap();
            CreateMap<Report, ReportViewModel>().ReverseMap();
            CreateMap<Comment, CommentViewModel>().ReverseMap();
            CreateMap<ServiceFeedback, ServiceFeedbackViewModel>().ReverseMap();
            CreateMap<Price, PriceViewModel>().ReverseMap();
            CreateMap<Price, UpdatePriceViewModel>().ReverseMap();
            CreateMap<BirdService, ServiceViewModel>().ReverseMap();
            CreateMap<BirdService, CreateServiceViewModel>().ReverseMap();
            CreateMap<ServiceCategory, ServiceCategoryViewModel>().ReverseMap();
            CreateMap<BirdServiceBooking, BirdServiceBookingViewModel>().ReverseMap();
            CreateMap<BirdServiceBooking, CreateBookingViewModel>().ReverseMap();
            CreateMap<BirdServiceBooking, UpdateBookingViewModel>().ReverseMap();
            CreateMap<BirdService, UpdateServiceViewModel>().ReverseMap();
            CreateMap<ServiceCategory, ServiceCategoryViewModel>().ReverseMap(); 
            CreateMap<MiniService, MiniServiceViewModel>().ReverseMap();
            CreateMap<MiniService, UpdateMiniServiceViewModel>().ReverseMap();
            CreateMap<Comment, GetCommentViewModel>().ReverseMap();
            CreateMap<Customer, GetCustomerViewModel>().ReverseMap();
            CreateMap<Customer, GetUserCustomerViewModel>().ReverseMap();
            CreateMap<Provider, GetProviderViewModel>().ReverseMap();
            CreateMap<Provider, GetUserProviderViewModel>().ReverseMap();
            CreateMap<Report, GetReportViewModel>().ReverseMap();
            CreateMap<Report, CreateReportViewModel>().ReverseMap();
            CreateMap<Report, UpdateReportViewModel>().ReverseMap();
            CreateMap<ServiceFeedback, GetServiceFeedbackViewModel>().ReverseMap();
            CreateMap<User, GetUserViewModel>().ReverseMap();
            CreateMap<BookingDetail, BookingDetailViewModel>().ReverseMap();
            CreateMap<BookingDetail, CreateBookingDetailViewModel>().ReverseMap();
            CreateMap<ServiceCategory, GetServiceCategoryViewModel>().ReverseMap();
            CreateMap<Price, FilterPriceViewModel>().ReverseMap();
            CreateMap<Price, CreatePriceViewModel>().ReverseMap();
            CreateMap<Price, AddPriceToBirdType>().ReverseMap();
            CreateMap<BirdService, PriceFilterBirdServiceViewModel>().ReverseMap();
            CreateMap<MiniService, CreateMiniServiceViewModel>().ReverseMap();
            CreateMap<Wallet, GetWalletViewModel>().ReverseMap();
            CreateMap<Transaction, CreateTransactionViewModel>().ReverseMap();
            CreateMap<Transaction, TransactionViewModel>().ReverseMap();
            CreateMap<Cart, CartViewModel>().ReverseMap();
            CreateMap<CartDetail, CartDetailViewModel>().ReverseMap();
            CreateMap<CartDetail, CreateCartDetailViewModel>().ReverseMap();
            CreateMap<CartDetail, UpdateCartDetailViewModel>().ReverseMap();
            CreateMap<BirdType, BirdTypeViewModel>().ReverseMap();
            CreateMap<BirdType, GetBirdTypeViewModel>().ReverseMap();
            CreateMap<BirdService, ServiceViewModelForBooking>().ReverseMap();
            CreateMap<BirdService, ServiceViewModelForCart>().ReverseMap();
        }
    }
}
