﻿using Applications;
using Applications.Interfaces;
using Applications.Repositories;
using Infrastructures.Repositories;
using System.Net.Http.Headers;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly ICurrentTimeService _currentTimeService;
        private readonly IClaimsServices _claimsServices;

        public UnitOfWork(AppDbContext context,
                          ICurrentTimeService currentTimeService,
                          IClaimsServices claimsServices)
        {
            _context = context;
            _currentTimeService = currentTimeService;
            _claimsServices = claimsServices;
        }

        public IUserRepo UserRepo => new UserRepo(_context, _currentTimeService, _claimsServices);

        public ICustomerRepo CustomerRepo => new CustomerRepo(_context, _currentTimeService, _claimsServices);

        public IProviderRepo ProviderRepo => new ProviderRepo(_context, _currentTimeService, _claimsServices);

        public IReportRepo ReportRepo => new ReportRepo(_context, _currentTimeService, _claimsServices);

        public ICommentRepo CommentRepo => new CommentRepo(_context, _currentTimeService, _claimsServices);

        public IServiceFeedbackRepo ServiceFeedbackRepo => new ServiceFeedbackRepo(_context, _currentTimeService, _claimsServices);

        public IBirdServiceRepo BirdServiceRepo => new BirdServiceRepo(_context, _currentTimeService, _claimsServices);

        public IBirdServiceBookingRepo BirdServiceBookingRepo => new BirdServiceBookingRepo(_context, _currentTimeService, _claimsServices);

        public IBookingDetailRepo BookingDetailRepo => new BookingDetailRepo(_context, _currentTimeService, _claimsServices);

        public IServiceCategoryRepo ServiceCategoryRepo => new ServiceCategoryRepo(_context, _currentTimeService, _claimsServices);

        public IPriceRepo PriceRepo => new PriceRepo(_context, _currentTimeService, _claimsServices);

        public IDashboardRepo DashboardRepo => new DashboardRepo(_context);

        public IMiniServiceRepo MiniServiceRepo => new MiniServiceRepo(_context, _currentTimeService, _claimsServices);

        public ITransactionRepo TransactionRepo => new TransactionRepo(_context, _currentTimeService, _claimsServices);

        public IWalletRepo WalletRepo => new WalletRepo(_context, _currentTimeService, _claimsServices);
        public ICartRepo CartRepo => new CartRepo(_context, _currentTimeService, _claimsServices);
        public ICartDetailRepo CartDetailRepo => new CartDetailRepo(_context, _currentTimeService, _claimsServices);

        public IBirdTypeRepo BirdTypeRepo => new BirdTypeRepo(_context, _currentTimeService, _claimsServices);

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}
