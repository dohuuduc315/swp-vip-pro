﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class MiniServiceConfig : IEntityTypeConfiguration<MiniService>
    {
        public void Configure(EntityTypeBuilder<MiniService> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Price)
                   .HasColumnType("money");

            builder.HasOne(x => x.BirdService)
                   .WithMany(x => x.MiniServices)
                   .HasForeignKey(x => x.BirdServiceId);

        }
    }
}
