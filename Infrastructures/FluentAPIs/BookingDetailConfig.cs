﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class BookingDetailConfig : IEntityTypeConfiguration<BookingDetail>
    {
        public void Configure(EntityTypeBuilder<BookingDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Price)
                   .HasColumnType("money");

            builder.HasOne(x => x.BirdServiceBooking)
                   .WithMany(x => x.BookingDetails)
                   .HasForeignKey(x => x.BirdServiceBookingId);

            builder.HasOne(x => x.BirdService)
                   .WithMany(x => x.BookingDetails)
                   .HasForeignKey(x => x.BirdServiceId);

            builder.HasOne(x => x.MiniService)
                   .WithMany(x => x.BookingDetails)
                   .HasForeignKey(x => x.MiniServiceId);
        }
    }
}
