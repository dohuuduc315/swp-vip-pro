﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class CartDetailConfig : IEntityTypeConfiguration<CartDetail>
    {
        public void Configure(EntityTypeBuilder<CartDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Price)
                   .HasColumnType("money");

            builder.HasOne(x => x.Cart)
                   .WithMany(x => x.CartDetails)
                   .HasForeignKey(x => x.CartId);

            builder.HasOne(x => x.BirdService)
                   .WithMany(x => x.CartDetails)
                   .HasForeignKey(x => x.BirdServiceId);
        }
    }
}
