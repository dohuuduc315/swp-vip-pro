﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class BirdServiceConfig : IEntityTypeConfiguration<BirdService>
    {
        public void Configure(EntityTypeBuilder<BirdService> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.ServiceCategory)
                   .WithMany(x => x.BirdServices)
                   .HasForeignKey(x => x.ServiceCategoryId);

            builder.HasOne(x => x.Provider)
                   .WithMany(x => x.BirdServices)
                   .HasForeignKey(x => x.ProviderId);

        }
    }
}
