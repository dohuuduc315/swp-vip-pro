﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class PriceConfig : IEntityTypeConfiguration<Price>
    {
        public void Configure(EntityTypeBuilder<Price> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.PriceAmount)
                   .HasColumnType("money");

            builder.HasOne(x => x.BirdService)
                   .WithMany(x => x.Prices)
                   .HasForeignKey(x => x.BirdServiceId);

            builder.HasOne(x => x.BirdType)
                   .WithMany(x => x.Prices)
                   .HasForeignKey(x => x.BirdTypeId);
        }
    }
}
