﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Customer)
                   .WithMany(x => x.Comments)
                   .HasForeignKey(x => x.CustomerId);

            builder.HasOne(x => x.Provider)
                   .WithMany(x => x.Comments)
                   .HasForeignKey(x => x.ProviderId);
        }
    }
}
