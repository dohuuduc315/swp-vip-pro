﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class BirdTypeConfig : IEntityTypeConfiguration<BirdType>
    {
        public void Configure(EntityTypeBuilder<BirdType> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
