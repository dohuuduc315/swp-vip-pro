﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    internal class BirdServiceBookingConfig : IEntityTypeConfiguration<BirdServiceBooking>
    {
        public void Configure(EntityTypeBuilder<BirdServiceBooking> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.TotalPrice)
                   .HasColumnType("money");

            builder.HasOne(x => x.Customer)
                   .WithMany(x => x.BirdServiceBookings)
                   .HasForeignKey(x => x.CustomerId);

            builder.HasOne(x => x.Provider)
                   .WithMany(x => x.BirdServiceBookings)
                   .HasForeignKey(x => x.ProviderId);
        }
    }
}
