﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ServiceFeedbackConfig : IEntityTypeConfiguration<ServiceFeedback>
    {
        public void Configure(EntityTypeBuilder<ServiceFeedback> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Customer)
                   .WithMany(x => x.ServiceFeedbacks)
                   .HasForeignKey(x => x.CustomerId);

            builder.HasOne(x => x.BirdService)
                   .WithMany(x => x.ServiceFeedbacks)
                   .HasForeignKey(x => x.BirdServiceId);
        }
    }
}
