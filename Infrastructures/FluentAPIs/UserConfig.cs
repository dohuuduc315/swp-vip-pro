﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Customer)
                   .WithOne(x => x.User)
                   .HasForeignKey<User>(x => x.CustomerId);

            builder.HasOne(x => x.Provider)
                   .WithOne(x => x.User)
                   .HasForeignKey<User>(x => x.ProviderId);

            builder.HasOne(x => x.Wallet)
                   .WithOne(x => x.User)
                   .HasForeignKey<User>(x => x.WalletId);
        }
    }
}
