﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class updateMiniservice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MiniServiceId",
                table: "BookingDetails",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MiniService",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MiniServiceName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<double>(type: "float", nullable: false),
                    Price = table.Column<decimal>(type: "money", nullable: false),
                    BirdServiceId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MiniService", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MiniService_BirdServices_BirdServiceId",
                        column: x => x.BirdServiceId,
                        principalTable: "BirdServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookingDetails_MiniServiceId",
                table: "BookingDetails",
                column: "MiniServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_MiniService_BirdServiceId",
                table: "MiniService",
                column: "BirdServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingDetails_MiniService_MiniServiceId",
                table: "BookingDetails",
                column: "MiniServiceId",
                principalTable: "MiniService",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingDetails_MiniService_MiniServiceId",
                table: "BookingDetails");

            migrationBuilder.DropTable(
                name: "MiniService");

            migrationBuilder.DropIndex(
                name: "IX_BookingDetails_MiniServiceId",
                table: "BookingDetails");

            migrationBuilder.DropColumn(
                name: "MiniServiceId",
                table: "BookingDetails");
        }
    }
}
