﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class UpdateBooking2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Destination",
                table: "Providers");

            migrationBuilder.DropColumn(
                name: "ServiceLocation",
                table: "BirdServices");

            migrationBuilder.AddColumn<int>(
                name: "Location",
                table: "BirdServices",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location",
                table: "BirdServices");

            migrationBuilder.AddColumn<string>(
                name: "Destination",
                table: "Providers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ServiceLocation",
                table: "BirdServices",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
