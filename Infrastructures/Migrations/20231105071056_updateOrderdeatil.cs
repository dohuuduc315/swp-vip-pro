﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class updateOrderdeatil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirdSize",
                table: "Prices");

            migrationBuilder.DropColumn(
                name: "ServiceEndDate",
                table: "BirdServiceBookings");

            migrationBuilder.DropColumn(
                name: "ServiceStartDate",
                table: "BirdServiceBookings");

            migrationBuilder.AddColumn<DateTime>(
                name: "ActualEndDate",
                table: "BookingDetails",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ActualStartDate",
                table: "BookingDetails",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "BookingDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ServiceEndDate",
                table: "BookingDetails",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ServiceStartDate",
                table: "BookingDetails",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualEndDate",
                table: "BookingDetails");

            migrationBuilder.DropColumn(
                name: "ActualStartDate",
                table: "BookingDetails");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "BookingDetails");

            migrationBuilder.DropColumn(
                name: "ServiceEndDate",
                table: "BookingDetails");

            migrationBuilder.DropColumn(
                name: "ServiceStartDate",
                table: "BookingDetails");

            migrationBuilder.AddColumn<int>(
                name: "BirdSize",
                table: "Prices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ServiceEndDate",
                table: "BirdServiceBookings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ServiceStartDate",
                table: "BirdServiceBookings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
