﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class miniservice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingDetails_MiniService_MiniServiceId",
                table: "BookingDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_MiniService_BirdServices_BirdServiceId",
                table: "MiniService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MiniService",
                table: "MiniService");

            migrationBuilder.RenameTable(
                name: "MiniService",
                newName: "MiniServices");

            migrationBuilder.RenameIndex(
                name: "IX_MiniService_BirdServiceId",
                table: "MiniServices",
                newName: "IX_MiniServices_BirdServiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MiniServices",
                table: "MiniServices",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingDetails_MiniServices_MiniServiceId",
                table: "BookingDetails",
                column: "MiniServiceId",
                principalTable: "MiniServices",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MiniServices_BirdServices_BirdServiceId",
                table: "MiniServices",
                column: "BirdServiceId",
                principalTable: "BirdServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingDetails_MiniServices_MiniServiceId",
                table: "BookingDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_MiniServices_BirdServices_BirdServiceId",
                table: "MiniServices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MiniServices",
                table: "MiniServices");

            migrationBuilder.RenameTable(
                name: "MiniServices",
                newName: "MiniService");

            migrationBuilder.RenameIndex(
                name: "IX_MiniServices_BirdServiceId",
                table: "MiniService",
                newName: "IX_MiniService_BirdServiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MiniService",
                table: "MiniService",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingDetails_MiniService_MiniServiceId",
                table: "BookingDetails",
                column: "MiniServiceId",
                principalTable: "MiniService",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MiniService_BirdServices_BirdServiceId",
                table: "MiniService",
                column: "BirdServiceId",
                principalTable: "BirdServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
