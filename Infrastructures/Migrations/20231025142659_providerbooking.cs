﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class providerbooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProviderId",
                table: "BirdServiceBookings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BirdServiceBookings_ProviderId",
                table: "BirdServiceBookings",
                column: "ProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_BirdServiceBookings_Providers_ProviderId",
                table: "BirdServiceBookings",
                column: "ProviderId",
                principalTable: "Providers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BirdServiceBookings_Providers_ProviderId",
                table: "BirdServiceBookings");

            migrationBuilder.DropIndex(
                name: "IX_BirdServiceBookings_ProviderId",
                table: "BirdServiceBookings");

            migrationBuilder.DropColumn(
                name: "ProviderId",
                table: "BirdServiceBookings");
        }
    }
}
