﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class fixprovider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BirdServices_Providers_ProviderId",
                table: "BirdServices");

            migrationBuilder.DropForeignKey(
                name: "FK_BirdServices_ServiceCategories_ServiceCategoryId",
                table: "BirdServices");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceCategoryId",
                table: "BirdServices",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ProviderId",
                table: "BirdServices",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_BirdServices_Providers_ProviderId",
                table: "BirdServices",
                column: "ProviderId",
                principalTable: "Providers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BirdServices_ServiceCategories_ServiceCategoryId",
                table: "BirdServices",
                column: "ServiceCategoryId",
                principalTable: "ServiceCategories",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BirdServices_Providers_ProviderId",
                table: "BirdServices");

            migrationBuilder.DropForeignKey(
                name: "FK_BirdServices_ServiceCategories_ServiceCategoryId",
                table: "BirdServices");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceCategoryId",
                table: "BirdServices",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProviderId",
                table: "BirdServices",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BirdServices_Providers_ProviderId",
                table: "BirdServices",
                column: "ProviderId",
                principalTable: "Providers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BirdServices_ServiceCategories_ServiceCategoryId",
                table: "BirdServices",
                column: "ServiceCategoryId",
                principalTable: "ServiceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
