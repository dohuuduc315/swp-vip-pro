﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class updateBirdType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirdType",
                table: "Prices");

            migrationBuilder.AddColumn<int>(
                name: "BirdTypeId",
                table: "Prices",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ActualEndDate",
                table: "BirdServices",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ActualStartDate",
                table: "BirdServices",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BirdTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BirdName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BirdSize = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BirdTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Prices_BirdTypeId",
                table: "Prices",
                column: "BirdTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Prices_BirdTypes_BirdTypeId",
                table: "Prices",
                column: "BirdTypeId",
                principalTable: "BirdTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Prices_BirdTypes_BirdTypeId",
                table: "Prices");

            migrationBuilder.DropTable(
                name: "BirdTypes");

            migrationBuilder.DropIndex(
                name: "IX_Prices_BirdTypeId",
                table: "Prices");

            migrationBuilder.DropColumn(
                name: "BirdTypeId",
                table: "Prices");

            migrationBuilder.DropColumn(
                name: "ActualEndDate",
                table: "BirdServices");

            migrationBuilder.DropColumn(
                name: "ActualStartDate",
                table: "BirdServices");

            migrationBuilder.AddColumn<int>(
                name: "BirdType",
                table: "Prices",
                type: "int",
                nullable: true);
        }
    }
}
