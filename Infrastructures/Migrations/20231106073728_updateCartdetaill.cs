﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    public partial class updateCartdetaill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CartDetails_BirdServiceBookings_BirdServiceBookingId",
                table: "CartDetails");

            migrationBuilder.DropIndex(
                name: "IX_CartDetails_BirdServiceBookingId",
                table: "CartDetails");

            migrationBuilder.DropColumn(
                name: "BirdServiceBookingId",
                table: "CartDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BirdServiceBookingId",
                table: "CartDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CartDetails_BirdServiceBookingId",
                table: "CartDetails",
                column: "BirdServiceBookingId");

            migrationBuilder.AddForeignKey(
                name: "FK_CartDetails_BirdServiceBookings_BirdServiceBookingId",
                table: "CartDetails",
                column: "BirdServiceBookingId",
                principalTable: "BirdServiceBookings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
