﻿namespace Domain.Enums
{
    public enum Status
    {
        Active, InActive
    }

    public enum BookingStatus
    {
        Waiting, AlreadyPaid, Cancel
    }

    public enum WorkingStatus
    {
        Waiting, Confirmed, CheckedIn, CheckedOut, Cancel
    }

    public enum ServiceType
    {
        Boarding, Grooming, HealthCare
    }

    public enum PriceType
    {
        PerDay
    }

    public enum SizeName
    {

    }

    public enum Role
    {
        Customer, Provider, Admin
    }

    public enum BirdSize
    {
        small, medium , large
    }

    public enum Location
    {
        None, Hanoi, HoChiMinh, DaNang, CanTho, HaiPhong, NgheAn, Hue
    }

    public enum TransactionStatus
    {
        WaitingForConFirm,
        Complete,
        Failed
    }

    public enum TransactionType
    {
        recharge,
        withdraw
    }

    public enum Bank
    {
        Vietcombank, VietinBank, VPBank, BIDV, ShinHanBank, TechcomBank, TPBank
    }

    public enum ReportStatus
    {
        Pending, Processing, Confirm, Decline
    }

}

