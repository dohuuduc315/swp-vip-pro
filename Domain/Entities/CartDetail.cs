﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class CartDetail : BaseEntity
    {
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string Description { get; set; }
        public int BirdServiceId { get; set; }
        public BirdService? BirdService { get; set; }
        public int? MiniServiceId { get; set; }
        public MiniService? MiniService { get; set; }
        public int Quantity { get; set; }
        public int CartId { get; set; }
        public Cart? Cart { get; set;}
        public decimal Price { get; set; }
        public int? PriceId { get; set; }
    }
}
