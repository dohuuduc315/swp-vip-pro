﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string AvatarURL { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string? Email { get; set; }
        public string Password { get; set; }
        public DateTime? DOB { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Gender { get; set; }
        public Status? Status { get; set; }
        public string? Image { get; set; }
        public string? CodeResetPassword { get; set; }
        public DateTime? ResetCodeExpires { get; set; }
        public Role? Role { get; set; }
        public DateTime CreatedAt { get; set; }
        public Customer? Customer { get; set; }
        public int? CustomerId { get; set; }
        public int? ProviderId { get; set; }
        public Provider? Provider { get; set; }
        public ICollection<Report>? Reports { get; set; }
        public Wallet? Wallet { get; set; }
        public int? WalletId { get; set; }
        public Cart? Cart { get; set; }
    }
}
