﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class Report : BaseEntity
    {
        public string ReportName { get; set; }
        public string Content { get; set; }
        public ReportStatus? ReportStatus { get; set; }
        public string? AdminAnswer { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }
    }
}
