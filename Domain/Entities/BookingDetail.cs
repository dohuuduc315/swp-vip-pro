﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class BookingDetail : BaseEntity
    {
        public DateTime ServiceStartDate { get; set; }
        public DateTime? ServiceEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public WorkingStatus? WorkingStatus { get; set; }
        public string Description { get; set; }
        public int BirdServiceBookingId { get; set; }
        public BirdServiceBooking? BirdServiceBooking { get; set; }
        public int BirdServiceId { get; set; }
        public BirdService? BirdService { get; set; }
        public int? MiniServiceId { get; set; }
        public MiniService? MiniService { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int? PriceId { get; set; }
    }
}
