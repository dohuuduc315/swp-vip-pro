﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class BirdType : BaseEntity
    {
        public string BirdName { get; set; }
        public BirdSize BirdSize { get; set; }
        public ICollection<Price?>? Prices { get; set; }
    }
}
