﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class Price : BaseEntity
    {
        public string PriceName { get; set; }
        public decimal PriceAmount { get; set; }
        public PriceType PriceType { get; set; }
        public int BirdServiceId { get; set; }
        public BirdService? BirdService { get; set; }
        public int BirdTypeId { get; set; }
        public BirdType? BirdType { get; set; }
    }
}
