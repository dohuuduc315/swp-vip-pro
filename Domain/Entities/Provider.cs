﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class Provider : BaseEntity
    {
        public string ProviderName { get; set; }
        public double? Rating { get; set; }
        public string? Description { get; set; }
        public Location? Location { get; set; }
        public DateTime CreatedAt { get; set; }
        public User? User { get; set; }
        public ICollection<Comment>? Comments { get; set; }
        //public ICollection<BirdServiceBooking>? BirdServiceBookings { get; set; }
        public ICollection<BirdService>? BirdServices { get; set; }
        public bool IsAuthorized { get; set;}
        public ICollection<BirdServiceBooking>? BirdServiceBookings { get; set; }
    }
}
