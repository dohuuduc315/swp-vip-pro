﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class BirdServiceBooking : BaseEntity
    {
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public decimal TotalPrice { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }
        public int ProviderId { get; set; }
        public Provider? Provider { get; set; }
        public ICollection<BookingDetail>? BookingDetails { get; set; }
    }
}
