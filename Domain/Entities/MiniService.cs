﻿using Domain.Base;

namespace Domain.Entities
{
    public class MiniService : BaseEntity
    {
        public string MiniServiceName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime? UpdateAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int BirdServiceId { get; set; }
        public BirdService? BirdService { get; set; }
        public ICollection<BookingDetail>? BookingDetails { get; set; }
    }
}
