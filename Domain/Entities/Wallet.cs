﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class Wallet : BaseEntity
    {
        public decimal WalletAmount { get; set; }
        public User? User { get; set; }
        public Bank? Bank { get; set; }
        public string? BankNumber { get; set; }
        public List<Transaction>? Transactions { get; set; }
    }
}
