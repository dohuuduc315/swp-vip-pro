﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class ServiceCategory : BaseEntity
    {
        public string CategoryName { get; set; }
        public ServiceType ServiceType { get; set; }
        public ICollection<BirdService>? BirdServices { get; set; }
    }
}
