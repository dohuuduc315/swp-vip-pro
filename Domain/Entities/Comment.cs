﻿using Domain.Base;

namespace Domain.Entities
{
    public class Comment : BaseEntity
    {
        public string CommentContent { get; set; }
        public Customer? Customer { get; set; }
        public int CustomerId { get; set; }
        public int ProviderId { get; set; }
        public Provider? Provider { get; set; }
        public double Rating { get; set; }
    }
}
