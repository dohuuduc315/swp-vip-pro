﻿using Domain.Base;

namespace Domain.Entities
{
    public class Customer : BaseEntity
    {
        public string CustomerName { get; set; }
        public User? User { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<BirdServiceBooking>? BirdServiceBookings { get; set; }
        public ICollection<Comment>? Comments { get; set; }
        public ICollection<ServiceFeedback>? ServiceFeedbacks { get; set; }
    }
}
