﻿using Domain.Base;
using Domain.Enums;

namespace Domain.Entities
{
    public class BirdService : BaseEntity
    {
        public string? BirdServiceName { get; set; }
        public string? VideoURL { get; set; }
        public string? ImageURL { get; set; }
        public Status Status { get; set; }
        public Location? Location { get; set; }
        public string? Description { get; set; }
        public int? ServiceCategoryId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public ServiceCategory? ServiceCategory { get; set; }
        public double? AvgRating { get; set; }
        public int? ProviderId { get; set; }
        public Provider? Provider { get; set; }
        public ICollection<BookingDetail>? BookingDetails { get; set; }
        public ICollection<CartDetail>? CartDetails { get; set; }
        public ICollection<Price>? Prices { get; set; }
        public ICollection<ServiceFeedback>? ServiceFeedbacks { get; set; }
        public ICollection<MiniService>? MiniServices { get; set; }
    }
}
