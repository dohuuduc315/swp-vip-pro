﻿using Domain.Base;

namespace Domain.Entities
{
    public class ServiceFeedback : BaseEntity
    {
        public string Content { get; set; }
        public int? CustomerId { get; set; }
        public Customer? Customer { get; set; }
        public int? BirdServiceId { get; set; }
        public BirdService? BirdService { get; set; }
        public double Rating { get; set; }
    }
}
